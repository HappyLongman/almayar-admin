<div class="sidebar" data-color="white" data-active-color="danger">
    <div class="logo">
        <a href="#" class="simple-text logo-mini">
            <div class="logo-image-small">
                <img src="{{ asset('paper') }}/img/logo.png">
            </div>
        </a>
        <a href="#" class="simple-text logo-normal">
            {{ __('AL MALYAR') }}
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
        <li class="{{ $elementActive == 'ngo' ? 'active' : '' }}">
                <a href="{{url('/shopusermain')}}">
                    <i class="nc-icon nc-single-02"></i>
                    <p>{{ __('Client') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'dashboard' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'home') }}">
                    <i class="nc-icon nc-cart-simple"></i>
                    <p>{{ __('Category') }}</p>
                </a>
            </li>
            
            <li class="{{ $elementActive == 'donate' ? 'active' : '' }}">
                <a href="{{url('/productmain')}}">
                    <i class="fa fa-product-hunt" aria-hidden="true"></i>
                    <p>{{ __('Product') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'discount' ? 'active' : '' }}">
                <a href="{{url('/discountmain')}}">
                <i class="fa fa-cart-arrow-down" aria-hidden="true"></i>
                
                    <p>{{ __('Discount') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'help' ? 'active' : '' }}">
                <a href="{{url('/offerMain')}}">
                <i class="fa fa-gift" aria-hidden="true"></i>
                    <p>{{ __('Offer') }}</p>
                </a>
            </li>
        </ul>
    </div>
</div>
