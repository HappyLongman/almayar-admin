@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'ngo'
])

@section('content')
<div class="content">
        <div class="row">
            <div class="col-md-12">
            <div class="row user_pagenation">
            <!-- <input type="button" id = "" class="btn btn-primary" value = 'Add Category'> -->
            <!-- <button type="button" class="btn btn-primary" >Action</button> -->
            <!-- <a href="{{ url('smsTest')}}" class="btn btn-primary"> Action</a> -->
            <div class = "pagenation_client">
                @for ($i = 0; $i < ceil($totalNum/10); $i++)
                    @if ($i == $pageNum)
                        <a href="{{ url('userPage/' . $i) }}" class="pageButton pageSelected">{{ $i + 1 }}</a>
                    @else
                        <a href="{{ url('userPage/' . $i) }}" class="pageButton">{{ $i + 1 }}</a>
                    @endif
                    
                @endfor
            </div>
        </div>

                <div class="card ">
                <div class="card-header">
                        <h5 class="title" style = "font-weight: 500;">Client</h5>
                        
                    </div>
                    <div class="card-body">
                    <table class="table">
                        <thead class=" text-primary">
                            <th>
                                No
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                Type
                            </th>
                            <th>
                                Expire Date
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Phone Number
                            </th>
                            
                            <th>
                                Status
                            </th>
                            <th>
                                Created At
                            </th>
                            <th>
                                View
                            </th>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($Shopuser as $temp)
                            <tr>
                                <td>
                                <?php echo $i++; ?>
                                </td>
                                <td>
                                {{$temp->u_f_name}}
                                </td>
                                <td>
                                {{$temp->u_type}}
                                </td>
                                <td>
                                {{date_format(date_create($temp->u_expire_date),"Y-m-d")}}
                                </td>
                                <td>
                                {{$temp->u_email}}
                                </td>
                                <td>
                                {{$temp->u_phone_number}}
                                </td>
                                <td class = "pro_des">
                                @if($temp->u_status == 'pending')
                                    <span class="badge badge-pill badge-warning" style = "color:white">Pending</span>
                                    @elseif($temp->u_status == 'cancelled')
                                    <span class="badge badge-pill badge-dark">Cancelled</span>
                                    @elseif($temp->u_status == 'approved')
                                    <span class="badge badge-pill badge-success approved">Approved</span>
                                    @elseif($temp->u_status == 'suspended')
                                    <span class="badge badge-pill badge-danger suspended">Suspended</span>
                                @endif
                                </td>
                                <td>
                                {{$temp->u_created_at}}
                                </td>
                                <td class = 'tdID'>
                                    <a href="{{ url('shopuserDetail/'.$temp->u_id) }}" class = "btn btn-primary btn-sm"><i class = "fa fa-eye"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                   </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        function viewUser(id){
            console.log(id)
        }
     $(document).ready(function() {
        //sort td start
        var ascending = false;
        function sortTable(a, b) {
            var A = $(a).children('td').eq(1).text().toUpperCase();
            var B = $(b).children('td').eq(1).text().toUpperCase();
            if (ascending) {
                if (A > B) return 1;
                if (A < B) return -1;
            } else {
                if (A > B) return -1;
                if (A < B) return 1;
            }
            return 0;

        }

        $(".sortname").click(function(){
        var rows = $(".table tbody  tr").get();
        rows.sort(sortTable);
        $.each(rows, function(index, row) {
            $(".table").children("tbody").append(row);
        });
        var dd = 1;
        $(".tdID").each(function(){
            $(this).text(dd++)
        })
        if (ascending) {
            ascending = false;
        } else {
            ascending = true;
        }
        })
        $(".sortcat").click(function(){
        var rows = $(".table tbody  tr").get();
        rows.sort(sortTableCat);
        $.each(rows, function(index, row) {
            $(".table").children("tbody").append(row);
        });
        var dd = 1;
        $(".tdID").each(function(){
            $(this).text(dd++)
        })
        if (ascending) {
            ascending = false;
        } else {
            ascending = true;
        }
        })
        function sortTableCat(a, b) {
            var A = $(a).children('td').eq(3).text().toUpperCase();
            var B = $(b).children('td').eq(3).text().toUpperCase();
            if (ascending) {
                if (A > B) return 1;
                if (A < B) return -1;
            } else {
                if (A > B) return -1;
                if (A < B) return 1;
            }
            return 0;

        }
        //sort td end
        
        $(".del_cat").click(function(){
                var ngo_id = $(this).parent().prev().prev().prev().children().next().val();
                var result = confirm("Want to delete?");
                // alert(ngo_id)
                if (result) {
                    demo.showNotification('top','right')
                    $(this).parent().parent().remove()
                    var dd = 1;
                    $(".tdID").each(function(){
                        $(this).text(dd++)
                    })
                        // $(this).parent().parent().hide();
                        $.ajax({
                        method:'POST',
                        url:'/ngos',
                        data:{ngo_id: ngo_id, action:'delete',"_token":"{{csrf_token()}}"},
                        success: function(res) {
                            // window.location.href = "ngomain";
                            // alert(res)
                        }
                    })
                }
        })
        $("#update_cat").click(function(){
            var selected = $( "#update_category option:selected" ).val();
            var ngo_val = $("#updateVal").val();
            var ngo_des = $("#updateDes").val();
            var ngo_id = $("#hiddenVal").val();
            // alert(selected+", "+ngo_val+",  "+ngo_id+ ",,ngo_des" +ngo_des)
            $.ajax({
                method:'POST',
                url:'/ngoUpdate',
                data:{ngo_id: ngo_id,ngo_des:ngo_des, selected:selected,ngo_val:ngo_val,"_token":"{{csrf_token()}}"},
                success: function(res) {
                    window.location.href = "ngomain";
                    // alert(res)
                }
            })
        })
        $(".update_cat").click(function(){
            var ngoName = $(this).parent().prev().prev().prev().text();
            var ngoDes = $(this).parent().prev().prev().text();
            var categoryName = $(this).parent().prev().text();
            var cat_id = $(this).parent().prev().prev().prev().children().val();
            var ngo_id = $(this).parent().prev().prev().prev().children().next().val();
            var categoryNameTrim = $.trim(categoryName)
            var ngoNameTrim = $.trim(ngoName)
            var ngoDesTrim = $.trim(ngoDes)
            // alert(ngoNameTrim + '   '+ ngoDesTrim +", categoryName"+categoryName+",cat_id"+cat_id+", ngo_id"+ngo_id)
            $("#updateVal").val(ngoNameTrim);
            $("#updateDes").val(ngoDesTrim);
            $("div select").val(cat_id);
            $("#hiddenVal").val(ngo_id);
            $('#myModal').modal({show:true});
        })
        $("#saveBtn").click(function(){
            var selected = $( "#select_category option:selected" ).val();
            var ngoDes = $("#ngo-des").val();
            var ngo_content = $("#message-text").val();
                $.ajax({
                    method:'POST',
                    url:'/ngos',
                    data:{ngo_content: ngo_content,ngo_des:ngoDes, selected:selected, action:'add',"_token":"{{csrf_token()}}"},
                    success: function(res) {
                        window.location.href = "ngomain";
                    }
                })
        })
     });
        function SelectText(element) {
            var doc = document,
                text = element,
                range, selection;
            if (doc.body.createTextRange) {
                range = document.body.createTextRange();
                range.moveToElementText(text);
                range.select();
            } else if (window.getSelection) {
                selection = window.getSelection();
                range = document.createRange();
                range.selectNodeContents(text);
                selection.removeAllRanges();
                selection.addRange(range);
            }
        }
        window.onload = function () {
            var iconsWrapper = document.getElementById('icons-wrapper'),
                listItems = iconsWrapper.getElementsByTagName('li');
            for (var i = 0; i < listItems.length; i++) {
                listItems[i].onclick = function fun(event) {
                    var selectedTagName = event.target.tagName.toLowerCase();
                    if (selectedTagName == 'p' || selectedTagName == 'em') {
                        SelectText(event.target);
                    } else if (selectedTagName == 'input') {
                        event.target.setSelectionRange(0, event.target.value.length);
                    }
                }

                var beforeContentChar = window.getComputedStyle(listItems[i].getElementsByTagName('i')[0], '::before').getPropertyValue('content').replace(/'/g, "").replace(/"/g, ""),
                    beforeContent = beforeContentChar.charCodeAt(0).toString(16);
                var beforeContentElement = document.createElement("em");
                beforeContentElement.textContent = "\\" + beforeContent;
                listItems[i].appendChild(beforeContentElement);

                //create input element to copy/paste chart
                var charCharac = document.createElement('input');
                charCharac.setAttribute('type', 'text');
                charCharac.setAttribute('maxlength', '1');
                charCharac.setAttribute('readonly', 'true');
                charCharac.setAttribute('value', beforeContentChar);
                listItems[i].appendChild(charCharac);
            }
        }
    </script>
@endpush