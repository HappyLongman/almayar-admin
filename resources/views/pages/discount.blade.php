@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'discount'
])

@section('content')
<div class="content">
    <div class="row">
            <div class="col-md-12">
                <div class = "header-container">
                    <a href="{{ url('discountAddProduct') }}" class="btn btn-primary">Add Discount</a>
                </div>

                
<!-- <div id="exampleModal" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header flex-column">
				<div class="icon-box">
					<i class="material-icons">&#xE5CD;</i>
				</div>						
				<h4 class="modal-title w-100">Are you sure?</h4>	
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<p class = "deleteConfirmTitle">Do you really want to delete these records? This process cannot be undone.</p>
			</div>
			<div class="modal-footer justify-content-center">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-danger deleteRecord">Delete</button>
			</div>
		</div>
	</div>
</div>      -->
<div class="modal fade" id="createFormId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header flex-column">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title w-100">Are you sure?</h4>	
                    
                </div>
            <div class="modal-body">
                <p class = "deleteConfirmTitle">Do you really want to delete these records? This process cannot be undone.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger btn-ok">Delete</a>
            </div>
        </div>
    </div>
</div>


                <div class="card">
                    <div class="card-header">
                        <h5 class="title">Discount</h5>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead class=" text-primary">
                            <!-- <th class="active">
                                <input type="checkbox" class="checkbox select-all checkbox" name="select-all" />
                            </th> -->
                                <th>
                                    No
                                </th>
                                <th>
                                    Type
                                </th>
                                <th>
                                    Amount
                                </th>
                                <th>
                                    Discription
                                </th>
                                <th>
                                    Product
                                </th>
                                <th>
                                    Owner
                                </th>
                                <th>
                                    From
                                </th>
                                <th>
                                    To
                                </th>
                                <th>
                                    Action
                                </th>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                @foreach($Discount as $temp)
                                <tr>
                                    <!-- <td class="active">
                                        <input type="checkbox" class="checkbox select-item checkbox" name="select-item" value="" />
                                    </td> -->
                                    <td class = "indexID">
                                        <input  id = "checkDeleteId_{{$temp->dis_id}}" type="text" hidden value = "checkDeleteId_{{$temp->dis_id}}">
                                        <div><?php echo $i++; ?></div>
                                    </td>
                                    <td>
                                    {{$temp->dis_type}}
                                    </td>
                                    @if($temp->dis_type == 'fixed')
                                    <td>
                                    ${{$temp->dis_mount}}
                                    </td>
                                    @elseif($temp->dis_type == 'percent')
                                    <td>
                                    {{$temp->dis_mount}}%
                                    </td>
                                    @endif
                                    <td class = "pro_des">
                                    {{$temp->dis_discription}}
                                    </td>
                                    <td>
                                    <div class="dropdown">
                                        <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Product List
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        @foreach($temp->pro_names as $p_name)
                                            <a class="dropdown-item" href="#">{{$p_name[0]->pro_name}}</a>
                                            <!-- <a class="dropdown-item" href="#">Inactive</a> -->
                                        @endforeach
                                        </div>
                                    </div>
                                        <!-- <select name="" id="">
                                        @foreach($temp->pro_names as $p_name)
                                        <option value="{{$p_name[0]->pro_name}}">{{$p_name[0]->pro_name}}</option>
                                        @endforeach
                                        </select> -->
                                    </td>
                                    @if($temp->dis_admin == 1)
                                    <td>
                                    <span class="badge badge-pill badge-danger draft">ADMIN</span>
                                    </td>
                                    @elseif($temp->u_f_name)
                                    <td>
                                    {{$temp->u_f_name}}
                                    </td>
                                    @endif
                                    <td>
                                    {{date_format(date_create($temp->dis_validate_from),"Y-m-d")}}
                                    </td>
                                    <td>
                                    {{date_format(date_create($temp->dis_validate_to),"Y-m-d")}}
                                    </td>
                                    <td class = 'tdID'>
                                        <a href="{{ url('veiwDetailDiscount/'.$temp->dis_id) }}" class = "update_cat"><i class = "fa fa-eye discount-icon-detail"></i></a>
                                        <a data-toggle="modal" data-target="#confirm-delete" id = "{{$temp->dis_id}}"  class = "announce"><i class = "fa fa-trash discount-icon-trash"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
    var delID;
$(document).ready(function(){
   $(".announce").click(function(){ // Click to only happen on announce links
     $("#cafeId").val($(this).attr('id'));
     delID = $(this).attr('id');
     $('#createFormId').modal('show');
   });
});
$(".btn-ok").click(function(){
    $('#createFormId').modal('hide');
    rID = parseInt(delID);
    
    $.ajax({
            method:'POST',
            url:'/veiwDiscountDel',
            data:{del_id: rID, "_token":"{{csrf_token()}}"},
            success: function(res) {
                // console.log(res)
               $("#checkDeleteId_"+delID).parent().parent().remove();
                indexID = 1;
                    $(".indexID").each(function(){
                        $(this).children().next().text(indexID++)
                        console.log($(this).children().next().text())
                    })
               demo.showNotification('top','right')
            },
            error: function(){
            console.log("error")
            }
        })
})
$(function(){
   $(".dropdown-menu").on('click', 'a', function(){
       $(this).parents('.dropdown').find('button').text($(this).text());
   });
});



</script>
@endpush