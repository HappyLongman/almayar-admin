@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'dashboard'
])

@section('content')
    <div class="content">
         <div class="row">
            <div class = "header-container">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Add Category</button>
                <div>
                @for ($i = 0; $i < ceil($totalNum/10); $i++)
                    @if ($i == $pageNum)
                        <a href="{{ url('categoryPage/' . $i) }}" class="pageButton pageSelected">{{ $i + 1 }}</a>
                    @else
                        <a href="{{ url('categoryPage/' . $i) }}" class="pageButton">{{ $i + 1 }}</a>
                    @endif
                    
                @endfor
                </div>
            </div>
            <!-- <input type="button" id = "" class="btn btn-primary" value = 'Add Category'> -->
            
        </div>
        <!-- delete confirm modal -->
    <div class="modal fade" id="createFormId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header flex-column">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title w-100">Are you sure?</h4>
                        <!-- <input type="text" id = "cafeId">	 -->
                        
                    </div>
                <div class="modal-body">
                    <p class = "deleteConfirmTitle">Do you really want to delete these records? This process cannot be undone.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>
    <!-- add category modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

            <!-- New categor input modal -->
            <form method="POST" action="/actioins" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">New Category</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!-- <form> -->
                        <div class="form-group">
                            <label for="message-text" id = "new_cat_name" class="col-form-label">Category Name:</label>
                            <input class="form-control" name = "category_name" id="message-text">
                        </div>
                        <div class="form-group">
                            <label for="message-text" id = "new_cat_des_label" class="col-form-label">Description:</label>
                            <textarea class="form-control" name = "category_des" value = "" id="new_cat_des"></textarea>
                        </div>
                        <!-- </form> -->
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6 imgUp">
                                <div class="imagePreview"></div>
                                <label class="btn btn-primary">
                                Upload<input name = 'image' type="file" id = 'fileUpload' class="uploadFile img" style="width: 0px;height: 0px;overflow: hidden;">
                                </label>
                            </div><!-- col-2 -->
                        </div><!-- row -->
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" id = "saveBtn"  class="btn btn-primary">Save</button>
                    </div>
                </form>
                 <!-- New categor input modal end -->
            </div>
        </div>
    </div>
        <div class="row">
        <div class="col-md-12">
                <div class="card card-stats">
                    <div class="card-header">
                        <h4 class="card-title"> Category</h4>
                        <!-- <p class="card-category"> Here is a subtitle for this table</p> -->
                    </div>

                    <div class="card-body">
                        <div class="tabl">
                            <table id = "cat_tbl" class="table">
                                <thead class=" text-primary">
                                    <th>
                                        No
                                    </th>
                                    <th class = 'catName'>
                                        <a class = "">Name<img src="{{ asset('paper/img/ic_arrow_drop_down_18px.svg') }}"></a>
                                    </th>
                                    <th>
                                        Image
                                    </th>
                                    <th>
                                        Created at
                                    </th>
                                    <th>
                                        Updated at
                                    </th>
                                    <th>
                                        Description
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($category as $temp)
                                    <tr>
                                        <td class = 'tdID'>
                                            <input type="text" value = "{{$temp->cat_id}}" hidden>
                                            <div><?php echo $i++; ?></div>
                                        </td>
                                        <td>
                                        {{$temp->cat_name}}
                                        </td>
                                        <td>
                                        @if($temp->cat_image) 
                                        <img style="width: 50px; height: 50px;" src="{{ asset('uploads') }}/{{$temp->cat_image}}" alt="">
                                        @elseif(!$temp->cat_image)
                                        <img style="width: 50px; height: 50px;" src="http://cliquecities.com/assets/no-image-e3699ae23f866f6cbdf8ba2443ee5c4e.jpg" alt="">
                                        @endif
                                            
                                        </td>
                                        <td>
                                        {{$temp->cat_created_at}}
                                        </td>
                                        <td>
                                        {{$temp->cat_updated_at}}
                                        </td>
                                        <td class = "cat_des">
                                        {{$temp->cat_description}}
                                        </td>
                                        <td>
                                           <a href="#" class = "update_cat" onclick = "edit({{$temp->cat_id}})"><img src="{{ asset('paper/img/pencil.svg') }}"></a>
                                           <a href="#ddd" class = "announce" id = '{{$temp->cat_id}}'><img src="{{ asset('paper/img/trash-simple.svg') }}"></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                  
                                </tbody>
                            </table>
                        <!-- update modal display -->
                        <form method="POST" action="/actioinsUpdate" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="modal" id="myModal">
                                <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h4 class="modal-title">Category</h4>    
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div><div class="container"></div>
                                    <div class="modal-body">
                                    <input class="form-control" name = "update_id" hidden value = "" id="hiddenVal"/>
                                    <input class="form-control" name = "update_name" value = "" id="updateVal"/>
                                    <br/>
                                    <textarea class="form-control" name = "category_des_update" value = "" id="update_cat_des"></textarea>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-6 imgUp">
                                                <div class="imagePreview" id = "imagePreview_update"></div>
                                                <label class="btn btn-primary">
                                                Upload<input name = 'image' type="file" id = 'fileUpload_update' class="uploadFile img" style="width: 0px;height: 0px;overflow: hidden;">
                                                </label>
                                            </div><!-- col-2 -->
                                        </div><!-- row -->
                                        
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" id = "saveBtn"  class="btn btn-primary">Update</button>
                                    </div>
                                
                                </div>
                                </div>
                            </div>
                            </form>
                        <!-- update modal display end -->
                    </div>
                </div>
            </div>
        </div>
         
        </div>
    </div>
@endsection

@push('scripts')
    <script>
    $(".btn-ok").click(function(){
        $('#createFormId').modal('hide');
        rID = parseInt(delID);

        $.ajax({
                method:'POST',
                url:'/actioins',
                data:{cat_id: rID, action:'delete',"_token":"{{csrf_token()}}"},
                success: function(res) {
                    // window.location.href = "home";
                    
                    indexID = 1;
                    $("#"+delID).parent().parent().remove();
                    $(".tdID").each(function(){
                        $(this).children().next().text(indexID++)
                        console.log($(this).children().next().text())
                    })
                    demo.showNotification('top','right')
                }
            })
        // $.ajax({
        //         method:'POST',
        //         url:'/veiwDiscountDel',
        //         data:{del_id: rID, "_token":"{{csrf_token()}}"},
        //         success: function(res) {
        //             // console.log(res)
        //         $("#checkDeleteId_"+delID).parent().parent().remove();
        //             indexID = 1;
        //                 $(".indexID").each(function(){
        //                     $(this).children().next().text(indexID++)
        //                     console.log($(this).children().next().text())
        //                 })
        //         demo.showNotification('top','right')
        //         },
        //         error: function(){
        //         console.log("error")
        //         }
        //     })
    })
        function edit (e){
            var cate = <?php echo json_encode($category); ?>
            
            var temp = ''
            cate.map((c, index)=>{
                if(c.cat_id == e){
                    temp = c
                }
            })
            var flagsUrl = '{{ URL::asset('/uploads') }}'+'/'+temp.cat_image;
            console.log(flagsUrl)
            var imageTemp = "http://cliquecities.com/assets/no-image-e3699ae23f866f6cbdf8ba2443ee5c4e.jpg"
            var indexOf = temp.cat_image.indexOf("http://cliquecities.com/assets/no-image-e3699ae23f866f6cbdf8ba2443ee5c4e.jpg");
            if(!temp.cat_image){
                // $("#imagePreview_update").attr("src","http://cliquecities.com/assets/no-image-e3699ae23f866f6cbdf8ba2443ee5c4e.jpg");
                $("#imagePreview_update").css("background-image","url(" + flagsUrl + ")");
                console.log("none")
            }
            else if(temp.cat_image){
                $("#imagePreview_update").css("background-image","url(" + flagsUrl + ")");
            }
            $("#updateVal").val(temp.cat_name);
            $("#update_cat_des").val(temp.cat_description);
            $("#hiddenVal").val(temp.cat_id);
            $('#myModal').modal({show:true});
        }
        function deleteRow(cat_id){
            // var cat_id = $(this);
            var dd = 1;
            
            
            var result = confirm("Want to delete?");
            if (result) {

                $(".tdID").each(function(){
                if($(this).children()[0].value == cat_id){
                    $(this).children().parent().parent().remove()
                }
            })

                    $.ajax({
                    method:'POST',
                    url:'/actioins',
                    data:{cat_id: cat_id, action:'delete',"_token":"{{csrf_token()}}"},
                    success: function(res) {
                        // window.location.href = "home";
                        $(".tdID").each(function(){
                            console.log($(this).children().next().text(dd++))
                        })
                        demo.showNotification('top','right')
                    }
                })
            }
        }
        $(document).ready(function() {
            $(".announce").click(function(){ // Click to only happen on announce links
                $("#cafeId").val($(this).attr('id'));
                delID = $(this).attr('id');
                $('#createFormId').modal('show');
            });

            //sort start
            var ascending = false;
            function sortTableCat(a, b) {
                var A = $(a).children('td').eq(1).text().toUpperCase();
                var B = $(b).children('td').eq(1).text().toUpperCase();
                if (ascending) {
                    if (A > B) return 1;
                    if (A < B) return -1;
                } else {
                    if (A > B) return -1;
                    if (A < B) return 1;
                }
                return 0;
            }
            $(".catName").click(function(){
                var rows = $(".table tbody  tr").get();
                rows.sort(sortTableCat);
                $.each(rows, function(index, row) {
                    $(".table").children("tbody").append(row);
                });
                var dd = 1;
                $(".tdID").each(function(){
                    $(this).text(dd++)
                })
                if (ascending) {
                    ascending = false;
                } else {
                    ascending = true;
                }
            })
            //display modal
            $('#exampleModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var recipient = button.data('whatever') // Extract info from data-* attributes
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('.modal-title').text('New Category')
                modal.find('.modal-body input').val(recipient)
            })
            //update category info
            $("#update_cat").click(function(){
                var cat_val = $("#updateVal").val();
                var cat_id = $("#hiddenVal").val();
                $.ajax({
                    method:'POST',
                    url:'/update',
                    data:{cat_id: cat_id, action:cat_val,"_token":"{{csrf_token()}}"},
                    success: function(res) {
                        window.location.href = "home";
                        // console.log(res)
                    }
                })
            })
            //update modal display
            // $(".update_cat").click(function(){

            //     var temp = $(this).parent().prev().prev().prev().prev().children();
            //     console.log(temp)

            //     var txtVal = $(this).parent().prev().prev().prev().prev().prev().text();
            //     var txtValDes = $(this).parent().prev().text();
                
            //     var cat_id = $(this).parent().prev().prev().prev().children().val();
            //     var trimTitle = $.trim(txtVal)
            //     var trimDes = $.trim(txtValDes)
                
            // })
        });
    </script>
@endpush