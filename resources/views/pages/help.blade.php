@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'help'
])

@section('content')
    <div class="content">
    <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="title">HELP</h5>
                    </div>
                    <div class="card-body">
                    <table class="table">
                        <thead class=" text-primary">
                            <th>
                                No
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                NGO
                            </th>
                            <th>
                                Action
                            </th>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                                @foreach($Help as $temp)
                                <tr>
                                    <td class = "tdID">
                                        <?php echo $i++; ?>
                                    </td>
                                    <td>
                                    <input type="text" value = "{{$temp->help_id}}" hidden/> 
                                    {{$temp->help_name}}
                                    </td>
                                    <td>
                                    {{$temp->help_email}}
                                    </td>
                                    <td>
                                    {{$temp->help_ngo_name}}
                                    </td>
                                    <td>
                                        <a href="#" class = "update_cat"><img src="{{ asset('paper/img/eye-19.svg') }}"></a>
                                        <a href="#" class = "del_cat"><img src="{{ asset('paper/img/trash-simple.svg') }}"></a>
                                    </td>
                                </tr>
                                @endforeach
                        </tbody>
                    </table>
                    <!-- help more display modal -->
                            <div class="modal" id="myModal">
                                <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h4 class="modal-title">HELP</h4>    
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div><div class="container"></div>
                                    <div class="modal-body">
                                    <div style = "display:flex">
                                        <div>
                                            <label for="help-name" id = "new_cat_name" class="col-form-label">Name:</label>
                                            <input class="col-md-12 form-control" id="help-name" readonly>
                                        </div>
                                        <div style = "margin-left:auto">
                                            <label for="help-email" id = "new_cat_email" class="col-form-label">Email:</label>
                                            <input class="col-md-12 form-control" id="help-email" readonly>
                                        </div>
                                    </div>

                                    <div style = "display:flex">
                                        <div>
                                            <label for="help-phone" id = "new_cat_phone" class="col-form-label">Phone Number:</label>
                                            <input class="col-md-12 form-control" id="help-phone" readonly>
                                        </div>
                                        <div style = "margin-left:auto">
                                            <label for="help-alt-phone" id = "new_cat_alt_phone" class="col-form-label">Alternate Phone Number:</label>
                                            <input class="col-md-12 form-control" id="help-alt-phone" readonly>
                                        </div>
                                    </div>

                                    <label for="help-ngo" id = "new_cat_ngo" class="col-form-label">NGO:</label>
                                    <input class="col-md-12 form-control" id="help-ngo" readonly>
                                    
                                    <label for="help-des" id = "new_cat_name" class="col-form-label">Description:</label>
                                    <textarea class="form-control" id="help-des" readonly></textarea>
                                    </div>
                                    <div class="row" style = "margin-left:0">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                        
                                        </div>
                                    </div>
                                    </div>
                                    <div class="modal-footer">
                                    <a href="#" data-dismiss="modal" class="btn">OK</a>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <!-- help more display modal end-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script>
     $(document).ready(function() {
    //delete help item
        $(".del_cat").click(function(){
            var help_id = $(this).parent().prev().prev().prev().children().val();
            //    alert($(this).parent().prev().prev().children().val())
            var result = confirm("Want to delete?");
            if(result){
                demo.showNotification('top','right')
                $(this).parent().parent().remove()
                var dd = 1;
                $(".tdID").each(function(){
                    $(this).text(dd++)
                })
                $.ajax({
                    method:'POST',
                    url:'/donateDel',
                    data:{help_id: help_id, action:'help',"_token":"{{csrf_token()}}"},
                    success: function(res) {
                        // window.location.href = "helpmain";
                        // alert(res)
                    }
                });
            }
        });
        //display detail help info
        $(".update_cat").click(function(){
            var more_id = $(this).parent().prev().prev().prev().children().val();
            $.ajax({
                method:'POST',
                url:'/donateDel',
                data:{more_id: more_id, action:'moreHelp',"_token":"{{csrf_token()}}"},
                success: function(res) {
                    // window.location.href = "helpmain";
                    var rst = JSON.parse(res);
                    $("#help-name").val(rst.help_name)
                    $("#help-ngo").val(rst.help_ngo_name)
                    $("#help-email").val(rst.help_email)
                    $("#help-alt-phone").val(rst.help_alt_phone)
                    $("#help-phone").val(rst.help_phone)
                    $("#help-des").val(rst.help_des)
                    // alert(rst.help_id)
                    $('#myModal').modal({show:true});
                }
            })
           
        })
        
     });
</script>
@endpush