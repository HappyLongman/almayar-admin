@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'ngo'
])
@section('content')
<div class="content">
    <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" style = "display:flex">
                    @foreach($shopuserDetail as $temps)
                        <h5 class="title">{{$temps->u_f_name}}    {{$temps->u_l_name}} <span style = "font-weight: 500;">Information</span></h5>
                    </div>
                    <div class="card-body">
                        <div class = "row">
                                <div class = "col-md-3">
                                    <img src="{{ asset('uploads')}}/react.png" alt="IMAGE" class = "avata_user">
                                </div>
                                <div class = "col-md-4 user-status-label">
                                    <br/>
                                    @if($temps->u_verified == 1)
                                    <label for="help-name" id = "new_cat_name" class="col-form-label phone_number_verified"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Phone number verified&nbsp;&nbsp;<i class="fa fa-check" aria-hidden="true"></i></label><br/>
                                    @elseif($temps->u_verified == 0)
                                    <label for="help-name" id = "new_cat_name" class="col-form-label phone_number_unverified"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Phone number verified</label><br/>
                                    @endif
                                    @if($temps->u_status == 'pending')
                                    <label for="help-name" id = "new_cat_name" class="col-form-label"><i class="fa fa-user-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Status &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="badge badge-pill badge-warning" style = "color:white">Pending</span></label><br/>
                                    @elseif($temps->u_status == 'cancelled')
                                    <label for="help-name" id = "new_cat_name" class="col-form-label"><i class="fa fa-user-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Status &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="badge badge-pill badge-dark">Cancelled</span></label><br/>
                                    @elseif($temps->u_status == 'approved')
                                    <label for="help-name" id = "new_cat_name" class="col-form-label user_approved"><i class="fa fa-user-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Status &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="badge badge-pill badge-success approved">Approved</span></label><br/>
                                    @elseif($temps->u_status == 'suspended')
                                    <label for="help-name" id = "new_cat_name" class="col-form-label"><i class="fa fa-user-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Status &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="badge badge-pill badge-danger suspended">Suspended</span></label><br/>
                                    @endif
                                    @if($temps->u_expire_date>=$server_time)
                                    <label for="help-name" id = "new_cat_name" class="col-form-label expire_date user_approved"><i class="fa fa-calendar-o"></i>&nbsp;&nbsp;&nbsp;Expire Date  &nbsp;&nbsp;&nbsp;<span>{{date_format(date_create($temps->u_expire_date),"Y-m-d")}}</span></label>
                                    @elseif($temps->u_expire_date<$server_time)
                                    <label for="help-name" id = "new_cat_name" class="col-form-label expire_date"><i class="fa fa-calendar-o"></i>&nbsp;&nbsp;&nbsp;Expire Date  &nbsp;&nbsp;&nbsp;<span>{{date_format(date_create($temps->u_expire_date),"Y-m-d")}}</span></label>
                                    @endif
                                </div>
                                <div class = "col-md-3"></div>
                                <div class = "col-md-2">
                                    <div class="dropdown">
                                        <a class="btn btn-info dropdown-toggle" href="javascript:;" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Status
                                            
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" onclick = "user_status_setting({{$temps->u_id}}, 'approved')" href="javascript:;">Approve</a>
                                            <a class="dropdown-item" onclick = "user_status_setting({{$temps->u_id}}, 'pending')" href="javascript:;">Pending</a>
                                            <a class="dropdown-item" onclick = "user_status_setting({{$temps->u_id}}, 'suspended')" href="javascript:;">Suspend</a>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class = "row">
                            <div class="col-md-6">
                                <br/>
                                <label for="help-name" id = "new_cat_name" class="col-form-label">First Name:</label>
                                <input class="col-md-12 form-control" id="help-name" readonly value = "{{$temps->u_f_name}}">
                                <label for="help-name" id = "new_cat_name" class="col-form-label">Last Name:</label>
                                <input class="col-md-12 form-control" id="help-name" readonly value = "{{$temps->u_l_name}}">
                                <label for="help-name" id = "new_cat_name" class="col-form-label">Birthday</label>
                                <input class="col-md-12 form-control" id="help-name" readonly value = "{{$temps->u_birth}}">
                                <label for="help-name" id = "new_cat_name" class="col-form-label">Address:</label>
                                <input class="col-md-12 form-control" id="help-name" readonly value = "{{$temps->u_street}}, {{$temps->u_city}}">
                            </div>
                            <div class="col-md-6">
                                <br/>   
                                <label for="help-name" id = "new_cat_name" class="col-form-label">Type:</label>
                                <input class="col-md-12 form-control" id="help-name" readonly value = "{{$temps->u_type}}">
                                <label for="help-name" id = "new_cat_name" class="col-form-label">Phone Number:</label>
                                <input class="col-md-12 form-control" id="help-name" readonly value = "{{$temps->u_phone_number}}">
                                <label for="help-name" id = "new_cat_name" class="col-form-label">Email</label>
                                <input class="col-md-12 form-control" id="help-name" readonly value = "{{$temps->u_email}}">
                                <label for="help-name" id = "new_cat_name" class="col-form-label">Created At:</label>
                                <input class="col-md-12 form-control" id="help-name" readonly value = "{{$temps->u_created_at}}">
                            </div>
                            @endforeach
                        </div>
                       
                    </div>
                    
            </div>
    </div>
</div>
@endsection
@push('scripts')
    <script>
        function user_status_setting(id, status){
            if(status == "approved"){
                $(".user-status-label label:nth-child(2)").next().next().replaceWith("<label for='help-name' id = 'new_cat_name' class='col-form-label user_approved'><i class='fa fa-user-o' aria-hidden='true'></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Status &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='badge badge-pill badge-success approved'>Approved</span></label>")
            }
            else if(status == "pending"){
                $(".user-status-label label:nth-child(2)").next().next().replaceWith("<label for='help-name' id = 'new_cat_name' class='col-form-label'><i class='fa fa-user-o' aria-hidden='true'></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Status &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='badge badge-pill badge-warning' style = 'color:white'>Pending</span></label>")
            }
            else if(status == "suspended"){
                $(".user-status-label label:nth-child(2)").next().next().replaceWith("<label for='help-name' id = 'new_cat_name' class='col-form-label'><i class='fa fa-user-o' aria-hidden='true'></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Status &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='badge badge-pill badge-danger suspended'>Suspended</span></label>")
            }
            $.ajax({ 
                method:'POST',
                url:'/userStatus',
                data:{u_id: id, u_status:status, "_token":"{{csrf_token()}}"},
                success: function(res) {
                    
                },
                error:function(){
                    console.log(id, status)
                }
            })
            
        }
        $(document).ready(function() {

            if( $(".user-status-label label:nth-child(2)").next().next().children().next().text() == "Cancelled"){
                console.log("change class name")
                $(".dropdown").find("a").addClass("disabled")
                // console.log($(".dropdown").find("a").attr("class").switchClass("nav-link dropdown-toggle", "nav-link dropdown-toggle disabled"))
            }
        })
    </script>
@endpush