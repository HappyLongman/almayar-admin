@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'donate'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
            <div class = "header-container">
            <div class="dropdown">
                <a class="btn btn-info dropdown-toggle" href="javascript:;" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Status
                </a>

                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item" onclick = "publish_setting()" href="javascript:;">PUBLISH</a>
                    <a class="dropdown-item" onclick = "draft_setting()" href="javascript:;">DRAFT</a>
                </div>
            </div>
                <div>
                @for ($i = 0; $i < ceil($totalNum/10); $i++)
                    @if ($i == $pageNum)
                        <a href="{{ url('productPage/' . $i) }}" class="pageButton pageSelected">{{ $i + 1 }}</a>
                    @else
                        <a href="{{ url('productPage/' . $i) }}" class="pageButton">{{ $i + 1 }}</a>
                    @endif
                    
                @endfor
                </div>
            </div>
            
                <div class="card">
                    <div class="card-header">
                        <h5 class="title">Product</h5>
                        
                    </div>
                    <div class="card-body">
                    <table class="table">
                        <thead class=" text-primary">
                        <th class="active">
                            <input type="checkbox" class="checkbox select-all checkbox" name="select-all" />
                        </th>
                            <th>
                                No
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                category
                            </th>
                            <th>
                                price
                            </th>
                            <th>
                                description
                            </th>
                            <th>
                                owner
                            </th>
                            <th>
                                type
                            </th>
                            <th>
                                url
                            </th>
                            <th>
                                status
                            </th>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($Product as $temp)
                            <tr>
                                <td class="active">
                                    <input type="text" value = "{{$temp->pro_id}}" hidden>
                                    <input type="checkbox" class="checkbox select-item checkbox" name="select-item" value="" />
                                </td>
                                <td>
                                <?php echo $i++; ?>
                                </td>
                                <td>
                                {{$temp->pro_name}}
                                </td>
                                <td>
                                {{$temp->cat_name}}
                                </td>
                                @if($temp->pro_price != null)
                                <td>
                                ${{$temp->pro_price}}
                                </td>
                                @elseif($temp->pro_price == null)
                                <td>
                                {{$temp->pro_price}}
                                </td>
                                @endif
                                <td class = "pro_des">
                                {{$temp->pro_description}}
                                </td>
                                <td>
                                {{$temp->u_f_name}}
                                </td>
                                <td>
                                {{$temp->pro_type}}
                                </td>
                                <td>
                                {{$temp->pro_url}}
                                </td>
                                <td class = 'tdID'>
                                    @if($temp->pro_status == 'publish')
                                    <span class="badge badge-pill badge-success publish">Publish</span>
                                    @elseif($temp->pro_status == 'draft')
                                    <span class="badge badge-pill badge-danger draft">Draft</span>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <!-- donate more display modal -->
                        <div class="modal" id="myModal">
                            <div class="modal-dialog modal-md">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Donate</h4>    
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="container"></div>
                                <div class="modal-body">
                                    <div style = "display:flex">
                                        <div>
                                            <label for="donate-name" id = "new_cat_name" class="col-form-label">Name:</label>
                                            <input class="col-md-12 form-control" id="donate-name" readonly>
                                        </div>
                                        <div style = "margin-left:auto">
                                            <label for="donate-email" id = "new_cat_email" class="col-form-label">Email:</label>
                                            <input class="col-md-12 form-control" id="donate-email" readonly>
                                        </div>
                                    </div>

                                    <div style = "display:flex">
                                        <div>
                                            <label for="donate-phone" id = "new_cat_phone" class="col-form-label">Phone Number:</label>
                                            <input class="col-md-12 form-control" id="donate-phone" readonly>
                                        </div>
                                        <div style = "margin-left:auto">
                                            <label for="donate-alt-phone" id = "new_cat_alt_phone" class="col-form-label">Alternate Phone Number:</label>
                                            <input class="col-md-12 form-control" id="donate-alt-phone" readonly>
                                        </div>
                                    </div>
                                    <div style = "display:flex">
                                        <div>
                                            <label for="donate-ngo" id = "new_cat_ngo" class="col-form-label">NGO:</label>
                                            <input class="col-md-12 form-control" id="donate-ngo" readonly>
                                        </div>
                                        <div style = "margin-left:auto">
                                            <label for="donate-type" id = "new_cat_type" class="col-form-label">Type:</label>
                                            <input class="col-md-12 form-control" id="donate-type" readonly>
                                        </div>
                                    </div>
                                    <label for="donate-address" id = "new_cat_address" class="col-form-label">Address:</label>
                                    <input class="form-control" id="donate-address" readonly>
                                    <label for="donate-des" id = "new_cat_name" class="col-form-label">Description:</label>
                                    <textarea class="form-control" id="donate-des" readonly></textarea>
                                    </div>
                                    <div class="row" style = "margin-left:0">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <a href="#" data-dismiss="modal" class="btn">OK</a>
                                </div>
                            </div>
                            </div>
                        </div>
                <!-- donate more display modal end-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script>
    function publish_setting(){
        var publish_arr = [];
        $("input.select-item").each(function(index, item){
            if(item.checked == true){
                $(this).parent().next().next().next().next().next().next().next().next().next().children().remove();
                $(this).parent().next().next().next().next().next().next().next().next().next().append("<span class='badge badge-pill badge-success publish'>Publish</span>")
                publish_arr.push($(this).parent().children().val())
            }
        })
        $.ajax({ 
                method:'POST',
                url:'/productPublish',
                data:{product_id: JSON.stringify(publish_arr) ,"_token":"{{csrf_token()}}"},
                success: function(res) {
                    console.log(res)
                }
        })
        console.log("publish:",publish_arr)
    }
    function draft_setting(){
        var draft_arr = [];
        $("input.select-item").each(function(index, item){
            
            if(item.checked == true){
                $(this).parent().next().next().next().next().next().next().next().next().next().children().remove();
                $(this).parent().next().next().next().next().next().next().next().next().next().append("<span class='badge badge-pill badge-danger draft'>Draft</span>")
                draft_arr.push($(this).parent().children().val())
            }
        })
        $.ajax({ 
                method:'POST',
                url:'/productDraft',
                data:{product_id: JSON.stringify(draft_arr) ,"_token":"{{csrf_token()}}"},
                success: function(res) {
                    console.log(res)
                }
        })
        console.log("draft:",draft_arr)
    }
     $(document).ready(function() {
        
        $("input.select-all").click(function () {
            var checked = this.checked;
            $("input.select-item").each(function (index,item) {
                item.checked = checked;
            });
        });
        $("input.select-item").click(function () {
            var checked = this.checked;
            var all = $("input.select-all")[0];
            var total = $("input.select-item").length;
            var len = $("input.select-item:checked:checked").length;
            all.checked = len===total;
        });
     });
</script>
@endpush