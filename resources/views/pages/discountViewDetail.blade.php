@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'discount'
])

@section('content')
<div class="content">
    <div class="row">
            <div class="col-md-12">
                <div class = "header-container">
                    <!-- <button onclick = "saveDiscount()" class="btn btn-primary">Save Discount</button> -->
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5 class="title">Discount Information</h5>
                    </div>
                    <div class="card-body"> 
                        <div class = "row">
                            @foreach($allPro as $temp)
                            <div class="col-md-3">
                                <label for="donate-name" id = "new_cat_name" class="col-form-label">Discount Type:</label>
                                <select readonly disabled class="form-control" id = "discount_type">
                                    <option value="fixed">Fixed</option>
                                    <option value="percent">Percent</option>
                                </select>
                                <label for="donate-name" id = "new_cat_name" class="col-form-label">Discount Amount:</label>
                                <input readonly class="col-md-12 form-control" value = "{{{$temp->dis_mount}}}" id="discount_amount"  data-bind="value:replyNumber" type = "number" required/>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group" style = "margin-bottom: 0 !important;margin-top: 10px;">
                                    <label class="control-label" for="date">From</label>
                                    <input readonly disabled class="form-control" value = "{{date_format(date_create($temp->dis_validate_from),'Y-m-d')}}" id="discount_date_from" name="date" placeholder="YYY-MM-DD" type="text" required/>
                                    <label class="control-label" for="date">To</label>
                                    <input readonly disabled class="form-control" value = "{{date_format(date_create($temp->dis_validate_to),'Y-m-d')}}" id="discount_date_to" name="date" placeholder="YYY-MM-DD" type="text" required/>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <label for="donate-des" id = "new_cat_name" class="col-form-label">Description:</label>
                                <textarea readonly class="form-control" id="discount_description" style = "height:100px;max-height: 100px !important">{{$temp->dis_discription}}</textarea>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <!-- <h5 class="title">Discount</h5> -->
                    </div>
                    <div class="card-body">
                        <div class = "row">
                        </div>
                        <div class = "row">
                            <div class="col-md-12">
                                <div class="tree_main">
                                <h6 class="pt-3 pl-3">Products List</h6>
                                <hr style = "width: 50%; margin: initial; margin-top:15px; margin-bottom:15px;">
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-10 discount-product-add">
                            @foreach($productIds as $pro)
                                @foreach($pro as $key=>$value)
                                <button type='button' class='btn closes' data-dismiss='alert' aria-label='Close'>{{$value}}</button>
                                @endforeach
                            @endforeach
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script>
var selectedId = []
function saveDiscount(){
    var discount_description = $("#discount_description").val();
    var discount_date_to = $("#discount_date_to").val();
    var discount_date_from = $("#discount_date_from").val();
    var discount_amount = $("#discount_amount").val();
    var discount_type = $("#discount_type").val();
    if(discount_date_to < discount_date_from){
        alert("Discount Date To must be bigger than Discount Date From")
        $("#discount_date_to").focus()
    }
    else{
    // console.log(discount_type, discount_description, discount_date_to, discount_amount,selectedId)
        $.ajax({ 
            method:'POST',
            url:'/discountAdminSave',
            data:{product_id: JSON.stringify(selectedId),discount_type: discount_type,discount_description:discount_description, discount_date_to:discount_date_to, discount_date_from:discount_date_from, discount_amount:discount_amount ,"_token":"{{csrf_token()}}"},
            success: function(res) {
                window.location.href = "discountmain";
            },
            error:function(){
                console.log('error')
            }
        })
    }
}

function checkItem(id){
    if($("#c_io_"+id).prop("checked") == true){
        if ($.inArray(id, selectedId) >= 0) {
            // break;
        }else {
            selectedId.push(id)
            var txt = $("#c_io_"+id).next().text();
            $(".discount-product-add").append("<input type = 'text' value = "+id+" id = 'delete_item_"+id+"' hidden><button type='button' class='btn closes' data-dismiss='alert' aria-label='Close'>"+txt+"<span onclick = 'closeItem("+id+")' aria-hidden='true' style = 'margin-left:10%;'>×</span> </button>")
        }
    }
    else{
        for( var i = 0; i < selectedId.length; i++){ 
    
            if ( selectedId[i] === id) { 

                selectedId.splice(i, 1); 
            }
            $("#delete_item_"+id).next().remove();
            $("#delete_item_"+id).remove();
        }
        console.log(selectedId)
    }
}
function closeItem(id){
    for( var i = 0; i < selectedId.length; i++){ 
    
        if ( selectedId[i] === id) { 

            selectedId.splice(i, 1); 
        }
        $("#delete_item_"+id).next().remove();
        $("#delete_item_"+id).remove();
        $('#c_io_'+id).prop('checked', false);
    }
    
}
$(document).ready(function () {
      var date_input=$('input[name="date"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);



      $(".plus").click(function () {
        $(this).toggleClass("minus").siblings("ul").toggle();
    })

    $("input[type=checkbox]").click(function () {
        
        //var sp = $(this).attr("id");
        //if (sp.substring(0, 4) === "c_bs" || sp.substring(0, 4) === "c_bf") {
            $(this).siblings("ul").find("input[type=checkbox]").prop('checked', $(this).prop('checked'));
            if($(this).prop('checked') == true){
                // alert($(this).attr("id"));
                var id = $(this).attr("id");
                var cat_id = $('#'+id).val();
                // console.log(cat_id.search('cat_'))
                if(cat_id.search('cat_') == 0){
                    $('#'+id).next().next().children().each(function(){
                        
                    })
                }
                
            }
        //} 
    })

    $("input[type=checkbox]").change(function () {
        var sp = $(this).attr("id");
        if (sp.substring(0, 4) === "c_io") {
            var ff = $(this).parents("ul[id^=bf_l]").attr("id");
            if ($('#' + ff + ' > li input[type=checkbox]:checked').length == $('#' + ff + ' > li input[type=checkbox]').length) {
                $('#' + ff).siblings("input[type=checkbox]").prop('checked', true);
                check_fst_lvl(ff);
            }
            else {
                $('#' + ff).siblings("input[type=checkbox]").prop('checked', false);
                check_fst_lvl(ff);
            }
        }

        if (sp.substring(0, 4) === "c_bf") {
            var ss = $(this).parents("ul[id^=bs_l]").attr("id");
            if ($('#' + ss + ' > li input[type=checkbox]:checked').length == $('#' + ss + ' > li input[type=checkbox]').length) {
                $('#' + ss).siblings("input[type=checkbox]").prop('checked', true);
                check_fst_lvl(ss);
            }
            else {
                $('#' + ss).siblings("input[type=checkbox]").prop('checked', false);
                check_fst_lvl(ss);
            }
        }
    });

})

function check_fst_lvl(dd) {
    //var ss = $('#' + dd).parents("ul[id^=bs_l]").attr("id");
    var ss = $('#' + dd).parent().closest("ul").attr("id");
    if ($('#' + ss + ' > li input[type=checkbox]:checked').length == $('#' + ss + ' > li input[type=checkbox]').length) {
        //$('#' + ss).siblings("input[id^=c_bs]").prop('checked', true);
        $('#' + ss).siblings("input[type=checkbox]").prop('checked', true);
    }
    else {
        //$('#' + ss).siblings("input[id^=c_bs]").prop('checked', false);
        $('#' + ss).siblings("input[type=checkbox]").prop('checked', false);
    }

}

function pageLoad() {
    $(".plus").click(function () {
        $(this).toggleClass("minus").siblings("ul").toggle();
    })
}
</script>

@endpush