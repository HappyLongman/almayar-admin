<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Storage;
use Illuminate\Support\Facades\Hash;
use App\Models\Product;
use App\Models\Category;

use Stripe\Charge;
use Stripe\Price;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Subscription;

class ApiController extends Controller
{
    //
    public $base_upload_url = "http://192.168.110.121:8000/images/";
    public $upload_url = "http://192.168.110.121:8000/uploads/";
    public function getVerifyCode(Request $request){
        $phoneNumber = $request->phoneNumber;
        
        if($phoneNumber){
            
            $token = rand(100000,999999);
            $tokens = DB::table('shopusers')
            ->where('u_sms_verify_code', '=', $token)
            ->get();
            while(!$tokens){
                $token = rand(100000,999999);
                $tokens = DB::table('shopusers')
                ->where('u_token', '=', $token)
                ->get();
            }
            $products = DB::table('shopusers')
            ->where('u_phone_number', '=', $phoneNumber)
            ->get();
            if(count($products)){
                DB::statement("UPDATE shopusers SET u_sms_verify_code = '{$token}' where u_phone_number = {$phoneNumber}");
            } else {
                $inputs = ['u_sms_verify_code' => $token, "u_phone_number"=>$phoneNumber, "u_verified"=>false];
                DB::table("shopusers")
                ->insert($inputs);
            }
            return array(
                "status"=> true,
                "msg"=>"We sent verify code to your sms, Please check",
                "data"=>$token
            );
            
        } else {
            return array(
                "status"=> true,
                "msg"=>"Something error, Please contact Admin"
            );
        }
    }

    public function phoneVerify(Request $request){
        $phoneNumber = $request->phoneNumber;
        $verifyCode = $request->verifyCode;
        $products = DB::table('shopusers')
            ->where('u_phone_number', '=', $phoneNumber)
            ->get();

        if(count($products)){
            $flag = false;
            foreach($products as $phone){
                if($phone->u_sms_verify_code == $verifyCode){
                    DB::statement("UPDATE shopusers SET u_verified = TRUE where u_phone_number = {$phoneNumber}");
                    $flag = true;
                }
            }
            if($flag){
                return array(
                    "status"=> true,
                    "msg"=>"Your phone Number is verified",
                );
            } else {
                return array(
                    "status"=> false,
                    "msg"=>"Verify code is not corrrect, try again",
                    "data"=>$verifyCode
                );
            }
        } else {
            return array(
                "status"=> false,
                "msg"=>"Your phone Number doesn't exist, Please send code again",
                "data"=>""
            );
        }
    }

    public function validationPhoneNumber($phone){
        // print_r($phone);
        if(preg_match("/^[0-9]{3}-[0-9]{4}-[0-9]{3}$/", $phone))
                return true;
        else    return false;
    }

    public function SignUpWithAvatar(Request $request){
        
        $files = $request->file('image');
        echo $request;
        $phoneNumber = $request->phoneNumber;
        $userName = $request->userName;
        $pwd = $request->pwd;

        $filename = substr($phoneNumber, 1).'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images/avatar'), $filename);

        $cDate = \Carbon\Carbon::now()->toDateTimeString();
        $pwd = Hash::make($pwd);
        DB::statement("UPDATE shopusers SET u_avatar = '{$filename}', u_status = 'approved', u_created_at = '{$cDate}',  u_updated_at = '{$cDate}' , u_name = '{$userName}', u_pwd = '{$pwd}'  where u_phone_number = {$phoneNumber}");

        return array(
            "status"=> true,
            "msg"=>"Sign up successed!",
            "data"=>$filename
        );
    }

    public function SignUp (Request $request){
        
        $phoneNumber = $request->phoneNumber;
        $userName = $request->userName;
        $pwd = $request->pwd;
        
        $cDate = \Carbon\Carbon::now()->toDateTimeString();
        $pwd = Hash::make($pwd);
        DB::statement("UPDATE shopusers SET u_status = 'approved', u_created_at = '{$cDate}',  u_updated_at = '{$cDate}' , u_name = '{$userName}', u_pwd = '{$pwd}'  where u_phone_number = {$phoneNumber}");

        return array(
            "status"=> true,
            "msg"=>"Sign up successed!",
            "data"=>''
        );
    }

    public function Login(Request $request){
        $user = DB::table('shopusers')
            ->where('u_phone_number', '=', $request->phoneNumber)
            ->limit(1)
            ->get();
        
        if(count($user)){
            foreach($user as $u){
                if(Hash::check($request->pwd, $u->u_pwd)){
                    $products = $this->getProducts(0);
                    $newMsg = $this->getNewMessages($u->u_id);
                    $newNotify = $this->getNewNotifications($u->u_id);
                    $topUsers = $this->TopUsers();
                    return json_encode(array(
                        "status"=> true,
                        "msg"=>"Success",
                        "data"=>array(
                            "products"=>$products,
                            "newMsg"=>count($newMsg),
                            "newNotification"=>count($newNotify),
                            "topUsers"=>$topUsers,
                            "userInfo"=>$u->u_id,
                            "cur_page"=>0
                        )
                    ));
                } else {
                    return array(
                        "status"=> false,
                        "msg"=>"Invalid Phone Number or Password, Please try again",
                        "data"=>""
                    );
                }
            }
        } else {
            return array(
                "status"=> false,
                "msg"=>"Invalid Phone Number or Password, Please try again2",
                "data"=>""
            );
        }
        
    }

    public function TopUsers(){
        $likes = DB::select("SELECT shopusers.* FROM (SELECT COUNT(u_id) AS likes, u_id FROM followings GROUP BY u_id ORDER BY likes DESC LIMIT 10) AS likes_users LEFT JOIN shopusers ON shopusers.u_id = likes_users.u_id");
        if(count($likes)){
            $result = array();
            foreach($likes as $like){
                if($like->u_id){
                    $result[] = array(
                        "u_id"=>$like->u_id,
                        "key"=>strval($like->u_id),
                        "u_avatar"=>$like->u_avatar?$this->base_upload_url."avatar/".$like->u_avatar:"",
                        "u_name"=>$like->u_name,
                    );
                }
            }
            return $result;
        } else {
            return false;
        }
    }
    public function getNewMessages($u_id){

        $notifications = DB::table('chats')
        ->where("to_id", "=", $u_id)
        ->where("read_at", "=", null)
        ->get();
        if(count($notifications)){
            return $notifications;
        } else {
            return array();
        }
    }

    public function getNewNotifications($u_id){
        $notifications = DB::table('notifications')
        ->where("notifiable_id", "=", $u_id)
        ->where("read_at", "=", null)
        ->get();
        if(count($notifications)){
            return $notifications;
        } else {
            return array();
        }
    }

    public function getProducts($page){
        if(!$page) $page = 0;
        $products = DB::table('products')
            ->join('shopusers', 'shopusers.u_id', '=', 'products.pro_owner')
            ->select('products.*', 'shopusers.*')
            ->where('products.pro_status', "=", 'publish')
            ->limit(10)->offset($page*10)
            ->get();

        if(count($products)){
            $result = array();
            foreach($products as $pro){
                $likes = DB::table('likes')
                ->select("*")
                ->where("pro_id", "=", $pro->pro_id)
                ->get();
                $discount = DB::select("SELECT * FROM discounts WHERE FIND_IN_SET(".$pro->pro_id.", SUBSTRING(dis_apply_to, 2, (LENGTH(dis_apply_to)-2))) AND dis_validate_from <= NOW() AND dis_validate_to >= NOW()");
                if(count($discount)){
                    if($discount[0]->dis_type == 'fixed'){
                        $discount[0]->amount = "$ ".$discount[0]->dis_mount;
                    } else {
                        $discount[0]->amount = $discount[0]->dis_mount."%";
                    }
                }
                $result[] = array(
                    'key'=> $pro->pro_id,
                    'imgUrl'=> $pro->pro_url?$this->upload_url.$pro->pro_url:$this->upload_url.'pro_temp.png',
                    'price'=> $pro->pro_price?$pro->pro_price:0,
                    'title'=>$pro->pro_name,
                    'type'=>$pro->pro_type,
                    'text'=> $pro->pro_description?$pro->pro_description:"",
                    'publishDate'=> $pro->pro_updated_at?$pro->pro_updated_at:"",
                    'avatar'=> $pro->u_avatar?$this->base_upload_url."avatar/".$pro->u_avatar:$pro->u_avatar,
                    'placeName'=> $pro->u_street&&$pro->u_city?$pro->u_street.", ".$pro->u_city:"",
                    'userName'=> $pro->u_name?$pro->u_name:"",
                    'u_id'=> $pro->u_id,
                    'likeCount'=> count($likes),
                    'likes'=>$likes,
                    'discount'=>count($discount)?$discount[0]:null,
                    'favorite'=> true,
                );
            }
            return $result;
        } else {
            return false;
        }
    }

    public function getProfileInfo (Request $request){
        
        $user_id = $request->user_id;
        $userInfo = DB::table('shopusers')
            ->select('*')
            ->where('u_id', '=', $user_id)
            ->get();
        $products = DB::table('products')
            ->join('shopusers', 'shopusers.u_id', '=', 'products.pro_owner')
            ->select('products.*', 'shopusers.*')
            ->where('products.pro_owner', '=', $user_id)
            ->get();
        $temp = array();
        if(count($products)){
            foreach($products as $pro){
                $likes = DB::table('likes')
                ->select("*")
                ->where("pro_id", "=", $pro->pro_id)
                ->get();
                $discount = DB::select("SELECT * FROM discounts WHERE FIND_IN_SET(".$pro->pro_id.", SUBSTRING(dis_apply_to, 2, (LENGTH(dis_apply_to)-2))) AND dis_validate_from <= NOW() AND dis_validate_to >= NOW()");
                if(count($discount)){
                    if($discount[0]->dis_type == 'fixed'){
                        $discount[0]->amount = "$ ".$discount[0]->dis_mount;
                    } else {
                        $discount[0]->amount = $discount[0]->dis_mount."%";
                    }
                }
                $pro->imgUrl = $pro->pro_url?$this->upload_url.$pro->pro_url:$this->upload_url.'pro_temp.png';
                $pro->pro_url = $pro->pro_url?$pro->pro_url:$this->upload_url.'pro_temp.png';
                $pro->pro_format_currency = '$ '.$pro->pro_price;
                $pro->key = $pro->pro_id;
                $pro->price = $pro->pro_price?$pro->pro_price:0;
                $pro->type = $pro->pro_type;
                $pro->title=$pro->pro_name;
                $pro->text= $pro->pro_description?$pro->pro_description:"";
                $pro->publishDate= $pro->pro_updated_at?$pro->pro_updated_at:"";
                $pro->avatar= $pro->u_avatar?$this->base_upload_url."avatar/".$pro->u_avatar:$pro->u_avatar;
                $pro->placeName= $pro->u_street&&$pro->u_city?$pro->u_street.", ".$pro->u_city:"";
                $pro->userName= $pro->u_name?$pro->u_name:"";
                $pro->u_id= $pro->u_id;
                $pro->likeCount= count($likes);
                $pro->likes=$likes;
                $pro->discount=count($discount)?$discount[0]:null;
                    // 'discount'=>count($discount)?$discount[0]:null,
                    // 'favorite'=> true,
                $temp[] = $pro;
            }
        }
        $followers = DB::table('followings')
        ->select('*')
        ->where('u_id', '=', $user_id)
        ->get();

        $followering = DB::table('followings')
        ->select('*')
        ->where('follower_id', '=', $user_id)
        ->get();
        if($userInfo[0]->u_avatar) $userInfo[0]->u_avatar = $this->base_upload_url."avatar/".$userInfo[0]->u_avatar;
        if(count($userInfo)){
            return array(
                "status"=> true,
                "msg"=>"",
                "data"=>array(
                    'info'=>$userInfo[0],
                    'products'=>$products,
                    'followers_num'=>count($followers),
                    'following_num'=>count($followering),
                    'followers'=>$followers
                )
            );
        } else {
            return array(
                "status"=> false,
                "msg"=>"Server Error!",
                "data"=>''
            );
        }
        
    }

    public function toggleLike (Request $request){
        $user_id = $request->user_id;
        $pro_id = $request->pro_id;
        
        $likes = DB::table('likes')
        ->select('*')
        ->where('u_id', '=', $user_id)
        ->where('pro_id', '=', $pro_id)
        ->get();
        if(count($likes)){
            DB::table('likes')
                ->where('u_id', '=', $user_id)
                ->where('pro_id', '=', $pro_id)
                ->delete();            
            $like = DB::table('likes')
            ->select('*')
            ->where('pro_id', '=', $pro_id)
            ->get();

            return array(
                "status"=> true,
                "msg"=>"Server Error!",
                "data"=>array(
                    'flag'=>false,
                    'likes'=>$like
                )
            );
        } else {
            $inputs = ['u_id' => $user_id, "pro_id"=>$pro_id];
            DB::table("likes")
            ->insert($inputs);
            $like = DB::table('likes')
            ->select('*')
            ->where('pro_id', '=', $pro_id)
            ->get();

            return array(
                "status"=> true,
                "msg"=>"Server Error!",
                "data"=>array(
                    'flag'=>true,
                    'likes'=>$like
                )
            );
        }
    }

    public function getNextPage (Request $request){
        $page = $request->pageNum;
        if(!$page) $page = 0;
        $products = $this->getProducts($page);
        $temp = array();
        if($products)
        foreach($products as $pro){
            $temp[] = $pro; 
        }
        return json_encode(array(
            "status"=> true,
            "msg"=>"Success",
            "data"=>array(
                'products'=>$temp,
                'cur_page'=>$page
            )
        ));
    }

    public function getCategories (Request $request){
        $Category = Category::all();

        return json_encode(array(
            "status"=> true,
            "msg"=>"Success",
            "data"=>$Category?$Category:[]
        ));
    }
    
    public function addProduct (Request $request){
        $category = $request->category;
        $pro_name = $request->pro_name;
        $pro_price = $request->pro_price;
        $pro_description = $request->pro_description;
        $user_id = $request->user_id;
        $pro_discount_type = strtolower($request->pro_discount_type);
        $pro_discount_amount = $request->pro_discount_amount;
        $pro_type = $request->pro_type;
        $valid_from = $request->valid_from;
        $valid_to = $request->valid_to;
        $cDate = \Carbon\Carbon::now()->toDateTimeString();
        $user_data = DB::table("shopusers")
                        ->where("u_id", "=", $user_id)
                        ->where("u_expire_date", ">", $cDate)
                        ->get();
                        
        if(count($user_data) == 0){
            return json_encode(array(
                "status"=> false,
                "msg"=>"Please upgrade your membership",
                "data"=>''
            ));
        }
        $files = $request->file('pro_media');
        if(!$files)
        {
            return json_encode(array(
                "status"=> false,
                "msg"=>"Publish Failed",
                "data"=>''
            ));
        }
        if($pro_type == 'image'){
            $filename = "product".round(microtime(true) * 1000).".jpg";
        } else {
            $filename = "product".round(microtime(true) * 1000).".mp4";
        }
        
        request()->pro_media->move(public_path('uploads/'), $filename);
        $inputs = ['cat_id' => $category, "pro_name"=>$pro_name, "pro_description"=>$pro_description, "pro_price"=>$pro_price, "pro_type"=>$pro_type, "pro_url"=>$filename, "pro_owner"=>$user_id, 'pro_status'=>'publish', 'pro_created_at'=>$cDate, 'pro_updated_at'=>$cDate];

        DB::table("products")
        ->insert($inputs);

        if($pro_discount_amount){
            $last2 = DB::table('products')->orderBy('pro_id', 'DESC')->first();
            $apply_to = [];
            $apply_to[] = $last2->pro_id;
           
            $valid_from = strtotime($valid_from);
            $valid_to = strtotime($valid_to);

            $inputs1 = [
                'dis_type' => $pro_discount_type,
                "dis_mount"=>$pro_discount_amount,
                "dis_owner_id"=>$user_id,
                "dis_validate_from"=>date('Y-m-d H:i:s', $valid_from),
                "dis_validate_to"=>date('Y-m-d H:i:s', $valid_to),
                "dis_apply_to"=>json_encode($apply_to),
            ];
            DB::table("discounts")
            ->insert($inputs1);
        }

        return json_encode(array(
            "status"=> true,
            "msg"=>"Success",
            "data"=>$user_data
        ));
    }

    public function editProduct (Request $request){
        $pro_id = $request->pro_id;
        $pro_status = $request->pro_status;
        $category = $request->category;
        $pro_name = $request->pro_name;
        $pro_price = $request->pro_price;
        $pro_description = $request->pro_description;
        $user_id = $request->user_id;
        $pro_discount_type = strtolower($request->pro_discount_type);
        $pro_discount_amount = $request->pro_discount_amount;
        $pro_type = $request->pro_type;
        $valid_from = $request->valid_from;
        $valid_to = $request->valid_to;
        $image_changed = $request->image_changed;

        $cDate = \Carbon\Carbon::now()->toDateTimeString();
        $user_data = DB::table("shopusers")
                        ->where("u_id", "=", $user_id)
                        ->where("u_expire_date", ">", $cDate)
                        ->get();
                        
        if(count($user_data) == 0){
            return json_encode(array(
                "status"=> false,
                "msg"=>"Please upgrade your membership",
                "data"=>''
            ));
        }
        
        DB::statement("UPDATE products SET cat_id = '{$category}', pro_name = '{$pro_name}', pro_description = '{$pro_description}', pro_price = '{$pro_price}', pro_status = '{$pro_status}', pro_updated_at='{$cDate}' where pro_id = {$pro_id}");
        

            // $last2 = DB::table('products')->orderBy('pro_id', 'DESC')->first();

        
        $apply_to = [];
        $apply_to[] = (int)$pro_id;

        if($pro_discount_amount){
            // $valid_from = strtotime($valid_from);
            // $valid_to = strtotime($valid_to);
            // $valid_from = new \Carbon\Carbon($valid_from);
            // $valid_to = new \Carbon\Carbon($valid_to);
            // return "Successffffffffffffffffffffffffffffffffffff";
            // \Carbon\Carbon::now()->toDateTimeString();
            // $valid_from = date("Y-m-d", $valid_from );
            // $valid_to = date("Y-m-d", $valid_to );
            $valid_from = strtotime($valid_from);
            $valid_to = strtotime($valid_to);
            $discount = DB::select("SELECT * FROM discounts WHERE FIND_IN_SET(".$pro_id.", SUBSTRING(dis_apply_to, 2, (LENGTH(dis_apply_to)-2)))");
            if($discount){
                DB::statement("UPDATE discounts SET dis_type = '{$pro_discount_type}', dis_mount = '{$pro_discount_amount}', dis_validate_from = '".date('Y-m-d H:i:s', $valid_from)."', dis_validate_to = '".date('Y-m-d H:i:s', $valid_to)."' WHERE FIND_IN_SET(".$pro_id.", SUBSTRING(dis_apply_to, 2, (LENGTH(dis_apply_to)-2)))");
            } else {
                $inputs1 = [
                    'dis_type' => $pro_discount_type,
                    "dis_mount"=>$pro_discount_amount,
                    "dis_owner_id"=>$user_id,
                    "dis_validate_from"=>date('Y-m-d H:i:s', $valid_from),
                    "dis_validate_to"=>date('Y-m-d H:i:s', $valid_to),
                    "dis_apply_to"=>json_encode($apply_to),
                ];
                DB::table("discounts")
                ->insert($inputs1);
            }
        } else {
            $discount = DB::select("SELECT * FROM discounts WHERE FIND_IN_SET(".$pro_id.", SUBSTRING(dis_apply_to, 2, (LENGTH(dis_apply_to)-2)))");
            if(count($discount)){
                DB::table("discounts")
                ->where("dis_id", "=", $discount[0]->dis_id)
                ->delete();
                // DB::statement("DELETE FROM discounts WHERE FIND_IN_SET(".$pro_id.", SUBSTRING(dis_apply_to, 2, (LENGTH(dis_apply_to)-2)))");
            }
        }

        return json_encode(array(
            "status"=> true,
            "msg"=>"Edit Product Success",
            "data"=>$this->getProfileInfoInternal($user_id)
        ));
    }

    public function editProductWithImage (Request $request){
        $pro_id = $request->pro_id;
        $pro_status = $request->pro_status;
        $category = $request->category;
        $pro_name = $request->pro_name;
        $pro_price = $request->pro_price;
        $pro_description = $request->pro_description;
        $user_id = $request->user_id;
        $pro_discount_type = strtolower($request->pro_discount_type);
        $pro_discount_amount = $request->pro_discount_amount;
        $pro_type = $request->pro_type;
        $valid_from = $request->valid_from;
        $valid_to = $request->valid_to;
        $image_changed = $request->image_changed;

        $cDate = \Carbon\Carbon::now()->toDateTimeString();
        $user_data = DB::table("shopusers")
                        ->where("u_id", "=", $user_id)
                        ->where("u_expire_date", ">", $cDate)
                        ->get();
                        
        if(count($user_data) == 0){
            return json_encode(array(
                "status"=> false,
                "msg"=>"Please upgrade your membership",
                "data"=>''
            ));
        }

        $files = $request->file('pro_media');
        if($pro_type == 'image'){
        $filename = "product".round(microtime(true) * 1000).".jpg";
        } else {
            $filename = "product".round(microtime(true) * 1000).".mp4";
        }
        
        request()->pro_media->move(public_path('uploads/'), $filename);

        DB::statement("UPDATE products SET cat_id = '{$category}', pro_name = '{$pro_name}', pro_description = '{$pro_description}', pro_price = '{$pro_price}', pro_type = '{$pro_type}', pro_url = '{$filename}',  pro_status = '{$pro_status}', pro_updated_at='{$cDate}' where pro_id = {$pro_id}");
           

            // $last2 = DB::table('products')->orderBy('pro_id', 'DESC')->first();
        $apply_to = [];
        $apply_to[] = (int)$pro_id;

        if($pro_discount_amount){
            $valid_from = strtotime($valid_from);
            $valid_to = strtotime($valid_to);
            $discount = DB::select("SELECT * FROM discounts WHERE FIND_IN_SET(".$pro_id.", SUBSTRING(dis_apply_to, 2, (LENGTH(dis_apply_to)-2)))");
            if($discount){
                DB::statement("UPDATE discounts SET dis_type = '{$pro_discount_type}', dis_mount = '{$pro_discount_amount}', dis_validate_from = '".date('Y-m-d H:i:s', $valid_from)."', dis_validate_to = '".date('Y-m-d H:i:s', $valid_to)."' WHERE FIND_IN_SET(".$pro_id.", SUBSTRING(dis_apply_to, 2, (LENGTH(dis_apply_to)-2)))");
            } else {
                $inputs1 = [
                    'dis_type' => $pro_discount_type,
                    "dis_mount"=>$pro_discount_amount,
                    "dis_owner_id"=>$user_id,
                    "dis_validate_from"=>date('Y-m-d H:i:s', $valid_from),
                    "dis_validate_to"=>date('Y-m-d H:i:s', $valid_to),
                    "dis_apply_to"=>json_encode($apply_to),
                ];
                DB::table("discounts")
                ->insert($inputs1);
            }
        } else {
            $discount = DB::select("SELECT * FROM discounts WHERE FIND_IN_SET(".$pro_id.", SUBSTRING(dis_apply_to, 2, (LENGTH(dis_apply_to)-2)))");
            if(count($discount)){
                DB::table("discounts")
                ->where("dis_id", "=", $discount[0]->dis_id)
                ->delete();
            }
        }

        return json_encode(array(
            "status"=> true,
            "msg"=>"Edit Product Success",
            "data"=>$this->getProfileInfoInternal($user_id)
        ));
    }

    public function getAllProducts (Request $request){
        $cat_id = $request->cat_id;
        $products = DB::table('products')
            ->join('shopusers', 'shopusers.u_id', '=', 'products.pro_owner')
            ->select('products.*', 'shopusers.*')
            ->where('products.cat_id', '=', $cat_id)
            ->get();

        if(count($products)){
            $result = array();
            foreach($products as $pro){
                $likes = DB::table('likes')
                ->select("*")
                ->where("pro_id", "=", $pro->pro_id)
                ->get();
                $discount = DB::select("SELECT * FROM discounts WHERE FIND_IN_SET(".$pro->pro_id.", SUBSTRING(dis_apply_to, 2, (LENGTH(dis_apply_to)-2))) AND dis_validate_from <= NOW() AND dis_validate_to >= NOW()");
                if(count($discount)){
                    if($discount[0]->dis_type == 'fixed'){
                        $discount[0]->amount = "$ ".$discount[0]->dis_mount;
                    } else {
                        $discount[0]->amount = $discount[0]->dis_mount."%";
                    }
                }
                $result[] = array(
                    'key'=> $pro->pro_id,
                    'imgUrl'=> $pro->pro_url?$this->upload_url.$pro->pro_url:$this->upload_url.'pro_temp.png',
                    'price'=> $pro->pro_price?$pro->pro_price:0,
                    'title'=>$pro->pro_name,
                    'type'=>$pro->pro_type,
                    'text'=> $pro->pro_description?$pro->pro_description:"",
                    'publishDate'=> $pro->pro_updated_at?$pro->pro_updated_at:"",
                    'avatar'=> $pro->u_avatar?$this->base_upload_url."avatar/".$pro->u_avatar:$pro->u_avatar,
                    'placeName'=> $pro->u_street&&$pro->u_city?$pro->u_street.", ".$pro->u_city:"",
                    'userName'=> $pro->u_name?$pro->u_name:"",
                    'u_id'=> $pro->u_id,
                    'likeCount'=> count($likes),
                    'likes'=>$likes,
                    'discount'=>count($discount)?$discount[0]:null,
                    'favorite'=> true,
                );
            }
            return json_encode(array(
                "status"=> true,
                "msg"=>"Success",
                "data"=>$result
            ));
        } else {
            return json_encode(array(
                "status"=> true,
                "msg"=>"Success",
                "data"=>[]
            ));
        }
    }
    public function toggleFollow (Request $request){
       
        $user_id = $request->u_id;
        $follower_id = $request->follower_id;
        $flag = $request->flag;
        
        if($flag == 'follow'){
            $inputs = ['u_id' => $user_id, "follower_id"=>$follower_id];
            $following = DB::table('followings')
                            ->where("u_id", '=', $user_id)
                            ->where("follower_id", "=", $follower_id)
                            ->get();
            if(count($following) == 0)
            DB::table("followings")
            ->insert($inputs);
        } else {
            DB::table('followings')
                ->where('u_id', '=', $user_id)
                ->where('follower_id', '=', $follower_id)
                ->delete();
        }

        $userInfo = DB::table('shopusers')
            ->select('*')
            ->where('u_id', '=', $user_id)
            ->get();
        $products = DB::table('products')
            ->join('shopusers', 'shopusers.u_id', '=', 'products.pro_owner')
            ->select('products.*', 'shopusers.*')
            ->where('products.pro_owner', '=', $user_id)
            ->get();
        $temp = array();
        if(count($products)){
            foreach($products as $pro){
                $likes = DB::table('likes')
                ->select("*")
                ->where("pro_id", "=", $pro->pro_id)
                ->get();
                $discount = DB::select("SELECT * FROM discounts WHERE FIND_IN_SET(".$pro->pro_id.", SUBSTRING(dis_apply_to, 2, (LENGTH(dis_apply_to)-2))) AND dis_validate_from <= NOW() AND dis_validate_to >= NOW()");
                if(count($discount)){
                    if($discount[0]->dis_type == 'fixed'){
                        $discount[0]->amount = "$ ".$discount[0]->dis_mount;
                    } else {
                        $discount[0]->amount = $discount[0]->dis_mount."%";
                    }
                }
                $pro->imgUrl = $pro->pro_url?$this->upload_url.$pro->pro_url:$this->upload_url.'pro_temp.png';
                $pro->pro_url = $pro->pro_url?$pro->pro_url:$this->upload_url.'pro_temp.png';
                $pro->pro_format_currency = '$ '.$pro->pro_price;
                $pro->type = $pro->pro_type;
                $pro->key = $pro->pro_id;
                $pro->price = $pro->pro_price?$pro->pro_price:0;
                $pro->title=$pro->pro_name;
                $pro->text= $pro->pro_description?$pro->pro_description:"";
                $pro->publishDate= $pro->pro_updated_at?$pro->pro_updated_at:"";
                $pro->avatar= $pro->u_avatar?$this->base_upload_url."avatar/".$pro->u_avatar:$pro->u_avatar;
                $pro->placeName= $pro->u_street&&$pro->u_city?$pro->u_street.", ".$pro->u_city:"";
                $pro->userName= $pro->u_name?$pro->u_name:"";
                $pro->u_id= $pro->u_id;
                $pro->likeCount= count($likes);
                $pro->likes=$likes;
                    // 'discount'=>count($discount)?$discount[0]:null,
                    // 'favorite'=> true,
                $temp[] = $pro;
            }
        }
        $followers = DB::table('followings')
        ->select('*')
        ->where('u_id', '=', $user_id)
        ->get();

        $followering = DB::table('followings')
        ->select('*')
        ->where('follower_id', '=', $user_id)
        ->get();

        if($userInfo[0]->u_avatar) $userInfo[0]->u_avatar = $this->base_upload_url."avatar/".$userInfo[0]->u_avatar;
        if(count($userInfo)){
            return array(
                "status"=> true,
                "msg"=>"",
                "data"=>array(
                    'info'=>$userInfo[0],
                    'products'=>$products,
                    'followers_num'=>count($followers),
                    'following_num'=>count($followering),
                    'followers'=>$followers
                )
            );
        } else {
            return array(
                "status"=> false,
                "msg"=>"Server Error!",
                "data"=>''
            );
        }
    }

    public function editProfile (Request $request){
       
        $u_name = $request->userName;
        $u_f_name = $request->userFName;
        $u_l_name = $request->userLName;
        $u_city = $request->userCity;
        $u_description = $request->userDesc;
        $u_id = $request->userId;

        $cDate = \Carbon\Carbon::now()->toDateTimeString();

        $filename = false;
        if($request->avatarFlag == 'change'){
            $filename = "avatar".$u_id.'.jpg';
            request()->avatar->move(public_path('images/avatar'), $filename);   
            DB::statement("UPDATE shopusers SET u_name = '{$u_name}', u_f_name = '{$u_f_name}',u_l_name = '{$u_l_name}', u_city = '{$u_city}', u_description='{$u_description}', u_avatar='{$filename}', u_updated_at = '{$cDate}' where u_id = {$u_id}"); 
        } else if($request->avatarFlag == 'remove'){
            DB::statement("UPDATE shopusers SET u_name = '{$u_name}', u_f_name = '{$u_f_name}',u_l_name = '{$u_l_name}', u_city = '{$u_city}', u_description='{$u_description}', u_avatar='', u_updated_at = '{$cDate}' where u_id = {$u_id}"); 
        } else {
            DB::statement("UPDATE shopusers SET u_name = '{$u_name}', u_f_name = '{$u_f_name}',u_l_name = '{$u_l_name}', u_city = '{$u_city}', u_description='{$u_description}', u_updated_at = '{$cDate}' where u_id = {$u_id}");
        }
        
        return json_encode(array(
            "status"=> true,
            "msg"=>"Edit Success",
            "data"=>$this->getUserData($u_id)
            ));
    }

    public function getUserData ($u_id){
        
        
        $userInfo = DB::table('shopusers')
            ->select('*')
            ->where('u_id', '=', $u_id)
            ->get();
        $products = DB::table('products')
            ->join('shopusers', 'shopusers.u_id', '=', 'products.pro_owner')
            ->select('products.*', 'shopusers.*')
            ->where('products.pro_owner', '=', $u_id)
            ->get();

        $temp = array();

        if(count($products)){
            foreach($products as $pro){
                $likes = DB::table('likes')
                ->select("*")
                ->where("pro_id", "=", $pro->pro_id)
                ->get();
                $discount = DB::select("SELECT * FROM discounts WHERE FIND_IN_SET(".$pro->pro_id.", SUBSTRING(dis_apply_to, 2, (LENGTH(dis_apply_to)-2))) AND dis_validate_from <= NOW() AND dis_validate_to >= NOW()");
                if(count($discount)){
                    if($discount[0]->dis_type == 'fixed'){
                        $discount[0]->amount = "$ ".$discount[0]->dis_mount;
                    } else {
                        $discount[0]->amount = $discount[0]->dis_mount."%";
                    }
                }

                $pro->imgUrl = $pro->pro_url?$this->upload_url.$pro->pro_url:$this->upload_url.'pro_temp.png';
                $pro->pro_url = $pro->pro_url?$pro->pro_url:$this->upload_url.'pro_temp.png';
                $pro->pro_format_currency = '$ '.$pro->pro_price;
                $pro->key = $pro->pro_id;
                $pro->price = $pro->pro_price?$pro->pro_price:0;
                $pro->type = $pro->pro_type;
                $pro->title=$pro->pro_name;
                $pro->text= $pro->pro_description?$pro->pro_description:"";
                $pro->publishDate= $pro->pro_updated_at?$pro->pro_updated_at:"";
                $pro->avatar= $pro->u_avatar?$this->base_upload_url."avatar/".$pro->u_avatar:$pro->u_avatar;
                $pro->placeName= $pro->u_street&&$pro->u_city?$pro->u_street.", ".$pro->u_city:"";
                $pro->userName= $pro->u_name?$pro->u_name:"";
                $pro->u_id= $pro->u_id;
                $pro->likeCount= count($likes);
                $pro->likes=$likes;
                    // 'discount'=>count($discount)?$discount[0]:null,
                    // 'favorite'=> true,
                $temp[] = $pro;
            }
        }

        $followers = DB::table('followings')
        ->select('*')
        ->where('u_id', '=', $u_id)
        ->get();

        $followering = DB::table('followings')
        ->select('*')
        ->where('follower_id', '=', $u_id)
        ->get();
        if($userInfo[0]->u_avatar) $userInfo[0]->u_avatar = $this->base_upload_url."avatar/".$userInfo[0]->u_avatar;
        if(count($userInfo)){
            return array(
                    'info'=>$userInfo[0],
                    'products'=>$products,
                    'followers_num'=>count($followers),
                    'following_num'=>count($followering),
                    'followers'=>$followers
                    );
        } else {
            return array();
        }
    }

    public function upgradeMembership (Request $request){
        \Stripe\Stripe::setApiKey('sk_test_51IPvrnIAaIJ9dA25lOk4dDB0RRLvkDYb1LF9pITxtcl67oiVpoHA3tycYgnUn01SDHmg8VAIzhOUaDfxV7JpMN4X00Odeh68UU');

        $c_token = $request->c_token;
        $member_month = $request->expire;
        $u_id = $request->u_id;

        $amount = $member_month == 1?5000:30000;
        $user = DB::table("shopusers")
                ->where("u_id", "=", $u_id)
                ->get();
        $charge = \Stripe\Charge::create(array(
                'amount'        => $amount,
                'currency'      => 'usd',
                'description'   => 'Membership upgrade('.$user[0]->u_phone_number.")",
                'source'        => $c_token
            ));
        
        if(isset($charge->status)){

            if($charge->status == "succeeded"){
                $cDate = \Carbon\Carbon::now();
                $eDate = \Carbon\Carbon::now();
                $eDate->addMonths($member_month);
    
                DB::statement("UPDATE shopusers SET u_expire_date = '{$eDate->toDateTimeString()}', u_updated_at = '{$cDate->toDateTimeString()}' where u_id = {$u_id}");
                return  array(
                    "status"=> true,
                    "msg"=>"Membership Upgrade Success",
                    "data"=>$charge
                );
            } else {
                return  array(
                    "status"=> false,
                    "msg"=>"Membership Upgrade is failed",
                    "data"=>$charge
                );
            }
        } else {
            return  array(
                "status"=> false,
                "msg"=>"Membership upgrade is failed",
                "data"=>""
            );
        }
    }

    public function getFollowers (Request $request){

        $u_id = $request->u_id;

        $followers = DB::table('followings')
        ->select('shopusers.*')
        ->join('shopusers', 'shopusers.u_id', '=', 'followings.follower_id')
        ->where('followings.u_id', '=', $u_id)
        ->get();
        $temp = array();
        foreach($followers as $fol){
            $fol->u_avatar = $fol->u_avatar?$this->base_upload_url."avatar/".$fol->u_avatar:"";
            $temp[] = $fol;
        }
        return  json_encode(array(
            "status"=> true,
            "msg"=>"Membership upgrade is failed",
            "data"=>$temp
        ));
    }

    public function getFollowings (Request $request){

        $u_id = $request->u_id;

        $followers = DB::table('followings')
        ->select('shopusers.*')
        ->join('shopusers', 'shopusers.u_id', '=', 'followings.u_id')
        ->where('followings.follower_id', '=', $u_id)
        ->get();
        $temp = array();
        foreach($followers as $fol){
            $fol->u_avatar = $fol->u_avatar?$this->base_upload_url."avatar/".$fol->u_avatar:"";
            $temp[] = $fol;
        }
        return  json_encode(array(
            "status"=> true,
            "msg"=>"Membership upgrade is failed",
            "data"=>$temp
        ));
    }

    public function getProfileInfoInternal ($user_id){
        
        $userInfo = DB::table('shopusers')
            ->select('*')
            ->where('u_id', '=', $user_id)
            ->get();
        $products = DB::table('products')
            ->join('shopusers', 'shopusers.u_id', '=', 'products.pro_owner')
            ->select('products.*', 'shopusers.*')
            ->where('products.pro_owner', '=', $user_id)
            ->get();
        $temp = array();
        if(count($products)){
            foreach($products as $pro){
                $likes = DB::table('likes')
                ->select("*")
                ->where("pro_id", "=", $pro->pro_id)
                ->get();
                $discount = DB::select("SELECT * FROM discounts WHERE FIND_IN_SET(".$pro->pro_id.", SUBSTRING(dis_apply_to, 2, (LENGTH(dis_apply_to)-2))) AND dis_validate_from <= NOW() AND dis_validate_to >= NOW()");
                if(count($discount)){
                    if($discount[0]->dis_type == 'fixed'){
                        $discount[0]->amount = "$ ".$discount[0]->dis_mount;
                    } else {
                        $discount[0]->amount = $discount[0]->dis_mount."%";
                    }
                }
                $pro->imgUrl = $pro->pro_url?$this->upload_url.$pro->pro_url:$this->upload_url.'pro_temp.png';
                $pro->pro_url = $pro->pro_url?$pro->pro_url:$this->upload_url.'pro_temp.png';
                $pro->pro_format_currency = '$ '.$pro->pro_price;
                $pro->key = $pro->pro_id;
                $pro->price = $pro->pro_price?$pro->pro_price:0;
                $pro->type = $pro->pro_type;
                $pro->title=$pro->pro_name;
                $pro->text= $pro->pro_description?$pro->pro_description:"";
                $pro->publishDate= $pro->pro_updated_at?$pro->pro_updated_at:"";
                $pro->avatar= $pro->u_avatar?$this->base_upload_url."avatar/".$pro->u_avatar:$pro->u_avatar;
                $pro->placeName= $pro->u_street&&$pro->u_city?$pro->u_street.", ".$pro->u_city:"";
                $pro->userName= $pro->u_name?$pro->u_name:"";
                $pro->u_id= $pro->u_id;
                $pro->likeCount= count($likes);
                $pro->likes=$likes;
                $pro->discount=count($discount)?$discount[0]:null;
                    // 'discount'=>count($discount)?$discount[0]:null,
                    // 'favorite'=> true,
                $temp[] = $pro;
            }
        }
        $followers = DB::table('followings')
        ->select('*')
        ->where('u_id', '=', $user_id)
        ->get();

        $followering = DB::table('followings')
        ->select('*')
        ->where('follower_id', '=', $user_id)
        ->get();
        if($userInfo[0]->u_avatar) $userInfo[0]->u_avatar = $this->base_upload_url."avatar/".$userInfo[0]->u_avatar;
        if(count($userInfo)){
            return array(
                    'info'=>$userInfo[0],
                    'products'=>$products,
                    'followers_num'=>count($followers),
                    'following_num'=>count($followering),
                    'followers'=>$followers
            );
        } else {
            return '';
        }
        
    }
    

    public function getMessage (Request $request){
        $u_id = $request->u_id;
        
        $messageList = DB::select("SELECT  DISTINCT 
        CASE WHEN to_id = $u_id
            THEN from_id 
            ELSE to_id 
        END userID
FROM    chats
WHERE $u_id IN (to_id, from_id)");
            

        $data = [];
        foreach ($messageList as $msg ){
            $user = DB::table('shopusers')
            ->where('u_id', $msg->userID)
            ->get();
            $message = DB::select("SELECT * FROM
                chats
                WHERE from_id=$u_id AND to_id = $msg->userID
                AND created_at = ( SELECT MAX(`created_at`) FROM chats WHERE from_id=$u_id )
                UNION All
                SELECT * FROM
                chats
                WHERE to_id=$u_id AND from_id = $msg->userID
                AND created_at = ( SELECT MAX(`created_at`) FROM chats WHERE to_id=$u_id )");

            if(!count($message)) $message = DB::select("SELECT * FROM (SELECT * 
            FROM chats
            WHERE (from_id = $msg->userID AND to_id = $u_id) OR (from_id = $msg->userID AND to_id = $u_id) ) AS msd
            WHERE msd.created_at = ( SELECT MAX(`created_at`) FROM chats WHERE (from_id = $msg->userID AND to_id = $u_id) OR (from_id = $msg->userID AND to_id = $u_id) )");

            $unRead = DB::select("SELECT COUNT(id) AS unread
            FROM chats
            WHERE from_id = $msg->userID AND to_id = $u_id AND read_at IS NULL");

            if(count($user)){
                $msg->avatar = $user[0]->u_avatar?$this->base_upload_url."avatar/".$user[0]->u_avatar:"";
                $msg->userName = $user[0]->u_name;
                $msg->userId = $user[0]->u_id;
                $msg->message = $message;
                $msg->unread = $unRead[0]->unread;
                $data[] = $msg;
            }
        }

        return  json_encode(array(
            "status"=> true,
            "msg"=>"Membership upgrade is failed",
            "data"=>$data
        ));
    }

    public function getPrivateMessage (Request $request){
        $u_id = $request->u_id;
        $client_id = $request->client_id;

        $message = DB::select("select * from chats where (from_id = $u_id and to_id = $client_id) or (to_id = $u_id and from_id = $client_id)");
        $client = DB::table("shopusers")
                    ->where("u_id", "=", $client_id)
                    ->get();
        if(count($client))
        $client[0]->avatar = $client[0]->u_avatar?$this->base_upload_url."avatar/".$client[0]->u_avatar:"";

        return  json_encode(array(
            "status"=> true,
            "msg"=>"Membership upgrade is failed",
            "data"=>array(
                "message"=>$message,
                "clientInfo"=>$client[0]
            )
        ));
    }

    public function searchUser (Request $request){
        $searchTxt = $request->searchTxt;
        $u_id = $request->u_id;
        $messageList = DB::select("SELECT u_id as userID
                            FROM `shopusers`
                            WHERE `u_name` LIKE '%{$searchTxt}%'");
        
        $data = [];
        foreach ($messageList as $msg ){
            if($msg->userID != $u_id){
                $user = DB::table('shopusers')
                ->where('u_id', $msg->userID)
                ->get();
                $message = DB::select("SELECT * FROM
                    chats
                    WHERE from_id=$u_id AND to_id = $msg->userID
                    AND created_at = ( SELECT MAX(`created_at`) FROM chats WHERE from_id=$u_id )
                    UNION All
                    SELECT * FROM
                    chats
                    WHERE to_id=$u_id AND from_id = $msg->userID
                    AND created_at = ( SELECT MAX(`created_at`) FROM chats WHERE to_id=$u_id )");
    
                if(!count($message)) $message = DB::select("SELECT * FROM (SELECT * 
                FROM chats
                WHERE (from_id = $msg->userID AND to_id = $u_id) OR (from_id = $msg->userID AND to_id = $u_id) ) AS msd
                WHERE msd.created_at = ( SELECT MAX(`created_at`) FROM chats WHERE (from_id = $msg->userID AND to_id = $u_id) OR (from_id = $msg->userID AND to_id = $u_id) )");
    
                $unRead = DB::select("SELECT COUNT(id) AS unread
                FROM chats
                WHERE from_id = $msg->userID AND to_id = $u_id AND read_at IS NULL");
    
                if(count($user)){
                    $msg->avatar = $user[0]->u_avatar?$this->base_upload_url."avatar/".$user[0]->u_avatar:"";
                    $msg->userName = $user[0]->u_name;
                    $msg->userId = $user[0]->u_id;
                    $msg->message = $message;
                    $msg->unread = $unRead[0]->unread;
                    $data[] = $msg;
                }
            }
        }

        return  json_encode(array(
            "status"=> true,
            "msg"=>"Membership upgrade is failed",
            "data"=>$data
        ));
    }
    
}

