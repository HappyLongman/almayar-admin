<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Category;
use DB;
use App\Models\Ngo;
use App\Discount;
use App\Models\Product;
use App\Models\Donate;
use App\Models\Help;
use App\Models\Shopuser;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use ElephantIO\Client as Elephant;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */

     //category table display
    public function index()
    {
        $Category = Category::all();
        $page = 0;
        $num = 10;
        $result = Category::limit($num)->offset($page)->get();
        return view("pages.dashboard")
            ->with('category',$result)
            ->with('pageNum', $page)
            ->with('totalNum', count($Category));
    }

    public function categoryPage($page){
        $Category = Category::all();
        $page = $page;
        $num = 10;
        $result = Category::limit($num)->offset($page*$num)->get();
        return view("pages.dashboard")
            ->with('category',$result)
            ->with('pageNum', $page)
            ->with('totalNum', count($Category));
    }
    //ngo table display
    public function shopuserMain(){
        $Shopuser = Shopuser::all(); 
        $page = 0;
        $num = 10;
        $result = Shopuser::limit($num)->offset($page*$num)->get();
        $users = DB::table('ngos')
            ->join('categories', 'ngos.parent_id', '=', 'categories.cat_id')
            ->select('ngos.*', 'categories.*')
            ->get();
            return view("pages.shopuser")
                ->with('Shopuser',$Shopuser)
                ->with('pageNum', $page)
                ->with('totalNum', count($Shopuser));
    }
    public function userPage($page){
        $Shopuser = Shopuser::all();
        $page = $page;
        $num = 10;
        $result = Shopuser::limit($num)->offset($page*$num)->get();
        return view("pages.shopuser")
            ->with('Shopuser',$result)
            ->with('pageNum', $page)
            ->with('totalNum', count($Shopuser));
    }
    // discount table display
    public function discountmain(){
        $Discount = Discount::all();
        // $getShopUser = Shopuser::where('u_id', $id)->get();
        $products = [];
        $users = DB::table('discounts')
            ->leftJoin('shopusers', 'discounts.dis_owner_id', '=', 'shopusers.u_id')
            ->select('discounts.*', 'shopusers.u_f_name')
            ->get();
            foreach($users as $cat){
                $cat->pro_ids = json_decode($cat->dis_apply_to);
                $products[] = $cat;
            }
            $result = array();
            foreach($products as $idArray){
                $temp = array();
                foreach($idArray->pro_ids as $id){
                    $tempR = DB::table('products')
                                ->select('pro_name')
                                ->where("pro_id", "=", $id)
                                ->get();
                    $temp[] = $tempR;
                }
                $idArray->pro_names = $temp;
                $result[] = $idArray;
            }
            // exit;
        return view("pages.discount")->with('Discount',$result);
    }
    public function discountAddProduct(){
        $Category = Category::all();
        $result = array();
        $pro = array();
        foreach($Category as $cat){
            $products = DB::table('products')
                ->where('cat_id', '=', $cat->cat_id)
                ->get();
            $result[]=array(
                "cat_id"=>$cat->cat_id?$cat->cat_id:0,
                "cat_name"=>$cat->cat_name?$cat->cat_name:"",
                "product" => $products
            );
        }
        return view("pages.discountAddProduct")->with("allPro", $result);
    }
    public function veiwDetailDiscount($id){
        $Category = Category::all();
        $result = array();
        $pro = array();
        foreach($Category as $cat){
            $products = DB::table('products')
                ->where('cat_id', '=', $cat->cat_id)
                ->get();
            $result[]=array(
                "cat_id"=>$cat->cat_id?$cat->cat_id:0,
                "cat_name"=>$cat->cat_name?$cat->cat_name:"",
                "product" => $products
            );
        }
        $tmp = DB::table('discounts')
                    ->select('discounts.*')
                    ->where("dis_id", "=", $id)
                    ->get();
                $productIds = [];
        foreach($tmp as $dis_info){
            foreach(json_decode($dis_info->dis_apply_to) as $val){
                $tmps = DB::table('products')
                ->select('products.pro_name')
                ->where("pro_id", "=", $val)
                ->get();
                foreach($tmps as $v){
                    $productIds[] = array(
                        $val => $v->pro_name);
                }
                
            }
            // print_r($productIds);exit;
            if($dis_info->dis_admin == 1){
                return view("pages.discountAdminViewDetail")->with("allData", $result)->with("allPro", $tmp)->with("productIds", $productIds);
            }
            else{
                return view("pages.discountViewDetail")->with("allPro", $tmp)->with("productIds", $productIds);
            }
        }
    }
 
    //donate table display
    public function productmain(){
        $Product = Product::all();
        $page = 0;
        $num = 10;
        $result = Product::limit($num)->offset($page)->get();


        $Products = DB::table('products')
            ->join('categories', 'products.cat_id', '=', 'categories.cat_id')
            ->join('shopusers', 'shopusers.u_id', '=', 'products.pro_owner')
            ->select('products.*', 'categories.cat_name', 'shopusers.u_f_name')
            ->get();
            // dd($users);exit;
        return view("pages.donate")
            ->with('Product',$Products)
            ->with('pageNum', $page)
            ->with('totalNum', count($Product));
    }
   
    
    public function productPage($page){
        $Product = Product::all();
        $page = $page;
        $num = 10;
        $result = Product::limit($num)->offset($page*$num)->get();
        return view("pages.donate")
            ->with('Product',$result)
            ->with('pageNum', $page)
            ->with('totalNum', count($Product));
    }
    //help table display
    public function mainHelp(){
        $Help = Help::all();
        return view("pages.help")->with('Help',$Help);
    }
    public function shopuserDetail($id){
        $getShopUser = Shopuser::where('u_id', $id)->get();
        $current_date_time = \Carbon\Carbon::now()->toDateTimeString();
        return view("pages.shopuserDetail")->with("shopuserDetail", $getShopUser)->with("server_time", $current_date_time);
    }
}
