<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ngo;
use App\Models\Category;
use DB;
use Illuminate\Support\Facades\Hash;

class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display all the static pages when authenticated
     *
     * @param string $page
     * @return \Illuminate\View\View
     */
    public function index(string $page)
    {

    //  select * from categories as a
        // left join ngos on a.cat_id = ngos.parent_id
        $Ngo = Ngo::all();
        $Category = Category::all();
        
        $users = DB::table('ngos')
            ->join('categories', 'ngos.parent_id', '=', 'categories.cat_id')
            ->select('ngos.*', 'categories.*')
            ->get();



        if($page == 'icons'){
            return view("pages.{$page}")->with('Ngo',$users)->with('Category', $Category);
        }
        else{
            if (view()->exists("pages.{$page}")) {
            return view("pages.{$page}");
            }
        return abort(404);
        }
        
    }
}
