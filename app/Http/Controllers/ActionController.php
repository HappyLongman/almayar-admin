<?php

namespace App\Http\Controllers;
use App\Models\Category;
use App\Models\Ngo;
use App\Models\Product;
use App\Models\Help;
use App\Discount;
use App\Offer;
use App\Models\Donate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
class ActionController extends Controller
{

    //CATEGORY PART

    //category insert and delete
    public function index(Request $request)
    {
        $cat =  new Category;
        $data = $request->action;
        $cat_id = $request->cat_id;
        //category delete
        if($cat_id){
            Category::where('cat_id', $cat_id)->delete();
            // Ngo::where('parent_id', $cat_id)->delete();
            return redirect('home');
            return back();
        }
        //category insert
        else
        {
            $category_name =  request()->category_name;
            $category_des =  request()->category_des;
            $files = request()->file('image');
            $current_date_time = \Carbon\Carbon::now()->toDateTimeString();
            $imageTemp = "";
            if(!$files){
                $inputs = ['cat_name' => $category_name, 'cat_created_at' => $current_date_time,'cat_updated_at' => $current_date_time, 'cat_description' => $category_des, 'cat_image' => $imageTemp];
                Category::insert($inputs);
                return redirect('home');
            }
                
            else

                $name=$files->getClientOriginalName();  
                $real_name = time().".png"; 
                $path = request()->file('image')->move(public_path('uploads'), $real_name);
                $paths = public_path($path);
                $inputs = ['cat_name' => $category_name, 'cat_created_at' => $current_date_time,'cat_updated_at' => $current_date_time, 'cat_description' => $category_des, 'cat_image' => $real_name];
                // print_r($inputs); exit;
                Category::insert($inputs);
                return redirect('home');
        }
       
    }
    public function productPublish(Request $request){
        $data = json_decode($request->product_id, true);
        foreach($data as $t){
            DB::statement("UPDATE products SET pro_status = 'publish' where pro_id = {$t}");
        }
        return json_encode($data);
        
    }
    public function productDraft(Request $request){
        $data = json_decode($request->product_id, true);
        foreach($data as $t){
            DB::statement("UPDATE products SET pro_status = 'draft' where pro_id = {$t}");
        }
        return json_encode($data);
        
    }
    
    //category update
    public function actioinsUpdate(Request $request){
        
        
        $category_des =  request()->category_des_update;
        $id = request()->update_id;
        $cat_name = $request->update_name;
        $files = request()->file('image');
        $update_date_time = \Carbon\Carbon::now()->toDateTimeString();
        
        if(!$files){
            DB::statement("UPDATE categories SET cat_name = '{$cat_name}', cat_updated_at = '{$update_date_time}',cat_description = '{$category_des}' where cat_id = {$id}");
            return redirect('home');
        }
        $name=$files->getClientOriginalName();  
        $real_name = time().".png"; 
        $path = request()->file('image')->move(public_path('uploads'), $real_name);
        $paths = public_path($path);
        DB::statement("UPDATE categories SET cat_name = '{$cat_name}', cat_updated_at = '{$update_date_time}',cat_description = '{$category_des}', cat_image = '{$real_name}' where cat_id = {$id}");
            return redirect('home');
    }


    public function veiwDiscountDel(Request $request){
        $del_id =  request()->del_id;
        Discount::where('dis_id', $del_id)->delete();
        // dd($id);exit;
        return redirect('discountmain');
            // return view("pages.discountAdminViewDetail")->with('allPro',$result);
    }
    //NGO SETTING PART
    public function ngoUpdate(Request $request){
        $ngo_id = $request->ngo_id;
        $ngo_des = $request->ngo_des;
        $ngo_val = $request->ngo_val;
        $selected = $request->selected;
        DB::statement("UPDATE ngos SET ngo_des = '{$ngo_des}', ngo_name = '{$ngo_val}', parent_id = '{$selected}' where ngo_id = {$ngo_id}");
        return $ngo_id;
    }
    //ngo add and delete
    public function ngos(Request $request){
        $data = $request->action;
        $ngo_content = $request->ngo_content;
        $ngo_id = $request->ngo_id;
        $ngo_des = $request->ngo_des;
        $parent_id = $request->selected;
        if($data == 'delete'){
            Ngo::where('ngo_id', $ngo_id)->delete();
            return $ngo_id;
        }
        elseif($data == 'add'){
            $inputs = ['ngo_name' => $ngo_content, 'ngo_des' => $ngo_des, 'parent_id' => $parent_id];
            Ngo::insert($inputs);
            return back();
        }
    }

    //client request part
    public function getcategory(Request $request){
        $Category = Category::all();
        return $Category;
    }
    public function getngo(Request $request){
        $id = $request->id;
        $getNgo = Ngo::where('parent_id', $id)->get();
        return $getNgo;
    }
    //donate recieve from client and store
    public  function donate(Request $request){
        $donate_name = $request->donate_name;
        $donate_address = $request->donate_address;
        $donate_email = $request->donate_email;
        $donate_ngo_name = $request->donate_ngo_name;
        $donate_phone = $request->donate_phone;
        $donate_alt_phone = $request->donate_alt_phone;
        $donate_des = $request->donate_des;
        $donate_cash = $request->donate_cash;
        $donate_product = $request->donate_product;
        $donate_service = $request->donate_service;
        $donate_other = $request->donate_other;
        if($donate_other == ""){
            $donate_other = 0;
        }
        if($donate_service == ""){
            $donate_service = 0;
        }
        if($donate_product == ""){
            $donate_product = 0;
        }
        if($donate_cash == ""){
            $donate_cash = 0;
        }
        $inputs = ['donate_name' => $donate_name, 'donate_address' => $donate_address, 'donate_email' => $donate_email,
        'donate_phone' => $donate_phone, 'donate_alt_phone' => $donate_alt_phone, 'donate_des' => $donate_des
        , 'donate_service' => $donate_service,'donate_ngo_name' =>$donate_ngo_name, 'donate_other' => $donate_other, 'donate_product' => $donate_product
        , 'donate_cash' => $donate_cash];
        Donate::insert($inputs);
    }
    // offer section
    public function offerMain(){
        $Offer = Offer::all();
        return view("pages.offer.offer_main")->with("Offer", $Offer);
    }
    //help recieve from client and store
    public function help(Request $request){
        $help_name = $request->help_name;
        $help_address = $request->help_address;
        $help_email = $request->help_email;
        $help_phone = $request->help_phone;
        $help_alt_phone = $request->help_alt_phone;
        $help_des = $request->help_des;
        $help_ngo_name = $request->help_ngo_name;
        // $help_image = $request->help_image;, 'help_image' => $help_image
        $inputs = ['help_name' => $help_name, 'help_ngo_name' => $help_ngo_name, 'help_address' => $help_address, 'help_email' => $help_email,
        'help_phone' => $help_phone, 'help_alt_phone' => $help_alt_phone, 'help_des' => $help_des];
        Help::insert($inputs);
    }
    //client request part end



    //donate & help row delete part
    public function donateDel(Request $request){
        $donate_id = $request->donate_id;
        $help_id = $request->help_id;
        $more_id = $request->more_id;
        $moreDonate_id = $request->moreDonate_id;
        if($request->action == "donate"){
            Donate::where('donate_id', $donate_id)->delete();
        }
        elseif($request->action == "moreHelp"){
            $more = Help::where('help_id',$more_id)->first();
            return $more->toJson();
        }
        elseif($request->action == "moreDonate"){
            $rst = Donate::where('donate_id',$moreDonate_id)->first();
            return $rst->toJson();
        }
        else{
            Help::where('help_id', $help_id)->delete();
        }
    }
    public function userStatus(Request $request){
        $u_id = $request->u_id;
        $u_status = $request->u_status;
        DB::statement("UPDATE shopusers SET u_status = '{$u_status}' where u_id = {$u_id}");
        return $u_status;
    }
    public function discountAdminSave(Request $request){
        $product_id = $request->product_id;
        $discount_type = $request->discount_type;
        $discount_description = $request->discount_description;
        $discount_date_to = $request->discount_date_to;
        $discount_date_from = $request->discount_date_from;
        $discount_amount = $request->discount_amount;
        $current_date_time = \Carbon\Carbon::now()->toDateTimeString();
        $inputs = ['dis_type' => $discount_type, 'dis_mount' => $discount_amount, 'dis_discription' => $discount_description, 'dis_validate_from' => $discount_date_from,
        'dis_validate_to' => $discount_date_to, 'dis_apply_to' => $product_id, 'dis_created_at' => $current_date_time,'dis_admin' => 1];
        Discount::insert($inputs);    
        return redirect('home');
    }
    public function discountAdminUpdate(Request $request){
        $product_id = $request->product_id;
        $discount_type = $request->discount_type;
        $discount_description = $request->discount_description;
        $discount_date_to = $request->discount_date_to;
        $discount_date_from = $request->discount_date_from;
        $discount_amount = $request->discount_amount;
        $update_date_time = \Carbon\Carbon::now()->toDateTimeString();
        $dis_id = $request->discount_id;
        // print_r($dis_id);exit;
        DB::statement("UPDATE discounts SET dis_type = '{$discount_type}', dis_mount = '{$discount_amount}', dis_discription = '{$discount_description}', dis_validate_from = '{$discount_date_from}', 
        dis_validate_to = '{$discount_date_to}', dis_apply_to = '{$product_id}', dis_updated_at = '{$update_date_time}' where dis_id = {$dis_id}");
        print_r($dis_id);exit;    
    }
    
}
