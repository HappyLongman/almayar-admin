/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 10.4.14-MariaDB : Database - dashboard
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dashboard` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `dashboard`;

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(30) DEFAULT NULL,
  `cat_image` varchar(250) DEFAULT NULL,
  `cat_created_at` timestamp NULL DEFAULT NULL,
  `cat_updated_at` timestamp NULL DEFAULT NULL,
  `cat_description` text DEFAULT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8;

/*Data for the table `categories` */

insert  into `categories`(`cat_id`,`cat_name`,`cat_image`,`cat_created_at`,`cat_updated_at`,`cat_description`) values 
(119,'bags','1612874277.png','2021-02-09 12:37:39','2021-02-10 19:48:44','werqwerwerwqerw'),
(120,'Ubuntu','1612936716.png','2021-02-10 05:58:36','2021-02-10 05:58:36','This instance uses your default (us-east-1) key pair to sign in.\r\n\r\nYou can download your default private key from the Account page.'),
(129,'Category','1612979671.png','2021-02-10 17:53:19','2021-02-10 17:54:31','div class=\"modal fade\" id=\"createFormId\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\r\n        <div class=\"modal-dialog\">\r\n            <div class=\"modal-content\">\r\n                <div class=\"modal-header flex-column\">\r\n                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\r\n                        <h4 class=\"modal-title w-100\">Are you sure?</h4>\r\n                        <!-- <input type=\"text\" id = \"cafeId\">	 -->\r\n                        \r\n                    </div>\r\n                <div class=\"modal-body\">\r\n                    <p class = \"deleteConfirmTitle\">Do you really want to delete these records? This process cannot be undone.</p>\r\n                </div>\r\n                <div class=\"modal-footer\">\r\n                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>\r\n                    <a class=\"btn btn-danger btn-ok\">Delete</a>\r\n                </div>\r\n            </div>\r\n        </div>');

/*Table structure for table `discounts` */

DROP TABLE IF EXISTS `discounts`;

CREATE TABLE `discounts` (
  `dis_id` int(11) NOT NULL AUTO_INCREMENT,
  `dis_type` varchar(50) DEFAULT NULL,
  `dis_mount` float DEFAULT NULL,
  `dis_discription` text DEFAULT NULL,
  `dis_owner_id` int(11) DEFAULT NULL,
  `dis_validate_from` timestamp NULL DEFAULT NULL,
  `dis_validate_to` timestamp NULL DEFAULT NULL,
  `dis_apply_to` text DEFAULT NULL,
  `dis_admin` int(11) DEFAULT NULL,
  `dis_created_at` timestamp NULL DEFAULT NULL,
  `dis_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`dis_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/*Data for the table `discounts` */

insert  into `discounts`(`dis_id`,`dis_type`,`dis_mount`,`dis_discription`,`dis_owner_id`,`dis_validate_from`,`dis_validate_to`,`dis_apply_to`,`dis_admin`,`dis_created_at`,`dis_updated_at`) values 
(4,'fixed',12,'A disabled option is unusable and un-clickable. The disabled attribute can be set to keep a user from selecting the option until some other condition has been met (like selecting a checkbox, etc.). Then, a JavaScript is required to remove the disabled value, and make the option',NULL,'2021-03-25 00:00:00','2021-04-10 00:00:00','[8,9]',1,'2021-02-09 17:29:50','2021-02-10 14:09:02'),
(15,'fixed',123,'23432423423',NULL,'2021-02-20 00:00:00','2021-03-05 00:00:00','[8,6,4,9]',1,'2021-02-10 13:38:57','2021-02-10 13:58:36');

/*Table structure for table `donates` */

DROP TABLE IF EXISTS `donates`;

CREATE TABLE `donates` (
  `donate_id` int(11) NOT NULL AUTO_INCREMENT,
  `donate_name` varchar(100) DEFAULT NULL,
  `donate_ngo_name` varchar(30) DEFAULT NULL,
  `donate_address` varchar(100) DEFAULT NULL,
  `donate_email` varchar(50) DEFAULT NULL,
  `donate_phone` varchar(30) DEFAULT NULL,
  `donate_alt_phone` varchar(30) DEFAULT NULL,
  `donate_cash` int(5) DEFAULT NULL,
  `donate_product` int(5) DEFAULT NULL,
  `donate_service` int(5) DEFAULT NULL,
  `donate_other` int(5) NOT NULL,
  `donate_des` text DEFAULT NULL,
  PRIMARY KEY (`donate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;

/*Data for the table `donates` */

insert  into `donates`(`donate_id`,`donate_name`,`donate_ngo_name`,`donate_address`,`donate_email`,`donate_phone`,`donate_alt_phone`,`donate_cash`,`donate_product`,`donate_service`,`donate_other`,`donate_des`) values 
(56,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL),
(57,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL),
(58,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL),
(59,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL),
(60,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL),
(61,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL),
(62,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL),
(63,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL),
(64,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL),
(65,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL),
(66,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL),
(67,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL),
(68,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL),
(69,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL),
(70,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL),
(71,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL),
(72,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL),
(73,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL),
(74,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL),
(75,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL);

/*Table structure for table `helps` */

DROP TABLE IF EXISTS `helps`;

CREATE TABLE `helps` (
  `help_id` int(11) NOT NULL AUTO_INCREMENT,
  `help_name` varchar(20) DEFAULT NULL,
  `help_ngo_name` varchar(20) DEFAULT NULL,
  `help_address` varchar(30) DEFAULT NULL,
  `help_email` varchar(30) DEFAULT NULL,
  `help_phone` varchar(30) DEFAULT NULL,
  `help_alt_phone` varchar(30) DEFAULT NULL,
  `help_des` text DEFAULT NULL,
  `help_image` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`help_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

/*Data for the table `helps` */

insert  into `helps`(`help_id`,`help_name`,`help_ngo_name`,`help_address`,`help_email`,`help_phone`,`help_alt_phone`,`help_des`,`help_image`) values 
(21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1),
(3,'2019_08_19_000000_create_failed_jobs_table',1),
(4,'2021_02_05_211740_create_notifications_table',2);

/*Table structure for table `ngos` */

DROP TABLE IF EXISTS `ngos`;

CREATE TABLE `ngos` (
  `ngo_id` int(11) NOT NULL AUTO_INCREMENT,
  `ngo_name` varchar(30) DEFAULT NULL,
  `ngo_des` text DEFAULT NULL,
  `parent_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`ngo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

/*Data for the table `ngos` */

insert  into `ngos`(`ngo_id`,`ngo_name`,`ngo_des`,`parent_id`) values 
(6,'ffffffffffffffff',NULL,71),
(24,'nice','good',73),
(27,'fsdf',NULL,73);

/*Table structure for table `notifications` */

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) unsigned NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `notifications` */

/*Table structure for table `offers` */

DROP TABLE IF EXISTS `offers`;

CREATE TABLE `offers` (
  `offer_id` int(11) NOT NULL AUTO_INCREMENT,
  `offer_title` text DEFAULT NULL,
  `offer_type` varchar(50) DEFAULT NULL,
  `offer_amount` float DEFAULT NULL,
  `offer_discription` text DEFAULT NULL,
  `offer_validate_from` timestamp NULL DEFAULT NULL,
  `offer_validate_to` timestamp NULL DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `offer_admin` int(11) DEFAULT NULL,
  `offer_image` varchar(250) DEFAULT NULL,
  `offer_dis_amount` float DEFAULT NULL,
  `offer_created_at` timestamp NULL DEFAULT NULL,
  `offer_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`offer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `offers` */

insert  into `offers`(`offer_id`,`offer_title`,`offer_type`,`offer_amount`,`offer_discription`,`offer_validate_from`,`offer_validate_to`,`owner_id`,`offer_admin`,`offer_image`,`offer_dis_amount`,`offer_created_at`,`offer_updated_at`) values 
(1,'Chinese New Year ','fixed',20,NULL,'2021-02-10 18:47:09','2021-02-20 18:47:11',1,NULL,NULL,122,'2021-02-10 18:47:25',NULL),
(2,'Weekend','percent',12,NULL,'2021-02-10 18:48:21','2021-02-19 18:48:23',2,NULL,NULL,333,'2021-02-10 18:48:37',NULL);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `pro_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) DEFAULT NULL,
  `pro_name` varchar(250) DEFAULT NULL,
  `pro_description` text DEFAULT NULL,
  `pro_price` float DEFAULT NULL,
  `pro_type` varchar(50) DEFAULT NULL,
  `pro_url` varchar(250) DEFAULT NULL,
  `pro_owner` varchar(250) DEFAULT NULL,
  `pro_status` varchar(50) DEFAULT NULL,
  `pro_created_at` timestamp NULL DEFAULT NULL,
  `pro_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pro_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

/*Data for the table `products` */

insert  into `products`(`pro_id`,`cat_id`,`pro_name`,`pro_description`,`pro_price`,`pro_type`,`pro_url`,`pro_owner`,`pro_status`,`pro_created_at`,`pro_updated_at`) values 
(3,70,'Clothes','gsdfgsdfgdsfgsdf',34,'image',NULL,'3','publish',NULL,NULL),
(4,117,'Media',NULL,NULL,NULL,NULL,'3','publish',NULL,NULL),
(5,119,'Sports',NULL,NULL,NULL,NULL,'4','publish',NULL,NULL),
(6,117,'Medicine',NULL,NULL,NULL,NULL,NULL,'publish',NULL,NULL),
(7,118,'Education',NULL,NULL,NULL,NULL,NULL,'publish',NULL,NULL),
(8,117,'Foods',NULL,NULL,NULL,NULL,NULL,'publish',NULL,NULL),
(9,117,'Glassess',NULL,NULL,NULL,NULL,NULL,'publish',NULL,NULL),
(21,323,'Phone',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `shopusers` */

DROP TABLE IF EXISTS `shopusers`;

CREATE TABLE `shopusers` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_f_name` varchar(50) DEFAULT NULL,
  `u_l_name` varchar(50) DEFAULT NULL,
  `u_birth` date DEFAULT NULL,
  `u_street` varchar(250) DEFAULT NULL,
  `u_city` varchar(250) DEFAULT NULL,
  `u_type` varchar(50) DEFAULT NULL,
  `u_expire_date` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `u_avatar` varchar(250) DEFAULT NULL,
  `u_verified` tinyint(1) DEFAULT NULL,
  `u_sms_verify_code` int(11) DEFAULT NULL,
  `u_status` varchar(50) DEFAULT NULL,
  `u_payment_info` varchar(250) DEFAULT NULL,
  `u_phone_number` int(11) DEFAULT NULL,
  `u_email` varchar(250) DEFAULT NULL,
  `u_created_at` timestamp NULL DEFAULT NULL,
  `u_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`u_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Data for the table `shopusers` */

insert  into `shopusers`(`u_id`,`u_f_name`,`u_l_name`,`u_birth`,`u_street`,`u_city`,`u_type`,`u_expire_date`,`u_avatar`,`u_verified`,`u_sms_verify_code`,`u_status`,`u_payment_info`,`u_phone_number`,`u_email`,`u_created_at`,`u_updated_at`) values 
(1,'cnr','cnr','2021-02-10','Astrey,37','Moscow','owner','2021-02-09 09:18:57',NULL,1,523421,'approved',NULL,1915209193,'cnr@yandex.com','2021-01-27 20:23:24',NULL),
(2,'CSN','ckr','2021-02-10','Astrey,37','Moscow','customer','2021-02-09 12:12:50',NULL,0,532342,'approved',NULL,1912321354,'cnr@yandex.com','2021-02-06 20:23:18',NULL),
(3,'ckr','ckr',NULL,'Astrey,37','Moscow','customer','2021-02-07 20:26:31',NULL,1,342334,'approved',NULL,1234214124,'cnr@yandex.com','2021-02-04 20:23:21',NULL),
(4,'ckr','ckr',NULL,'Astrey,37','Moscow','owner','2021-02-07 19:42:54',NULL,1,5323423,'cancelled',NULL,2147483647,'cnr@yandex.com','2021-01-27 20:23:15',NULL),
(6,NULL,NULL,NULL,NULL,NULL,NULL,'2021-02-10 20:58:09',NULL,0,NULL,'suspended',NULL,NULL,NULL,NULL,NULL),
(7,NULL,NULL,NULL,NULL,NULL,NULL,'2021-02-08 19:09:24',NULL,NULL,NULL,'pending',NULL,NULL,NULL,NULL,NULL),
(8,NULL,NULL,NULL,NULL,NULL,NULL,'2021-02-07 20:10:04',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(9,NULL,NULL,NULL,NULL,NULL,NULL,'2021-02-07 20:10:05',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(10,NULL,NULL,NULL,NULL,NULL,NULL,'2021-02-07 20:10:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(11,NULL,NULL,NULL,NULL,NULL,NULL,'2021-02-07 20:10:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(12,NULL,NULL,NULL,NULL,NULL,NULL,'2021-02-07 20:10:07',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(13,NULL,NULL,NULL,NULL,NULL,NULL,'2021-02-07 20:10:07',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(14,NULL,NULL,NULL,NULL,NULL,NULL,'2021-02-07 20:10:08',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(15,NULL,NULL,NULL,NULL,NULL,NULL,'2021-02-07 20:10:10',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(16,'fdgdfg','dfsdf',NULL,NULL,NULL,NULL,'2021-02-10 17:36:15',NULL,NULL,NULL,'pending',NULL,NULL,NULL,NULL,NULL),
(17,'fff','ff',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`updated_at`,`created_at`,`password`) values 
(1,'andrews','caloricpizza@yandex.ru','2020-12-16 11:21:54','2020-12-16 11:21:54','$2y$10$Y4bduFOc9C0LsNiP6CHrJe7Vg5bdFn6RtxJwiBX3JEVokB.kiEQdK'),
(2,'sdfdsaf','dsfasdfsadf@adfds','2020-12-16 12:18:09','2020-12-16 12:18:09','$2y$10$GNv7dlyhKwdzKwMA9se1V.HHz4KRZGnnXLlDfV34jT4pWG4vefVRq'),
(3,'boyka','aleksey.boyka@yandex.com','2020-12-21 10:51:27','2020-12-21 10:51:27','$2y$10$5A1SGdVtB596DqN7NOOgI.G6fNiMElxUdMCRcwiqhqBYOjs7rCNvG'),
(4,'admin','admin@admin.com','2021-02-04 08:02:06','2021-02-04 08:02:06','$2y$10$5A1SGdVtB596DqN7NOOgI.G6fNiMElxUdMCRcwiqhqBYOjs7rCNvG');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
