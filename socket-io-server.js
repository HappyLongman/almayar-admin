const express = require("express");
const bodyParser = require("body-parser");
// const cookieParser = require("cookie-parser");
const path = require("path");
const { getHeapCodeStatistics } = require("v8");
const app = express();
const PORT = 9090;
const axios = require("axios");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, "public")));
// app.use(cookieParser());

//setup server
const server = app.listen(PORT, function() {
  console.log("server running: "+PORT);
});
let userList = []

const io = require("socket.io")(server, {
  cors: {
    origin: '*',
  }
}); // socket setup

io.on("connection", function(socket) {
    console.log("new connection")
  socket.emit("sendMeID", "")

  socket.on('newUser', function(data){
    
    var flag = false
    userList.map((user, index)=>{
      if(user.user_id == data){
        flag = true
      }
    })

    if(!flag)
    userList.push({
      user_id: data,
      socket: socket
    })
    socket.broadcast.emit('sendStatus', {
      flag: true,
      to_id: data
    });
    console.log("user List", userList.length)
  })

  socket.on('disconnect', function(data) {
    
    console.log('disconnect..');
    var temp = []
    var userID = ''
    userList.map((user, index)=>{
      if(user.socket != socket){
        temp.push(user)
        userID = user.user_id
      }
    })

    socket.broadcast.emit('sendStatus', {
      flag: true,
      to_id: data
    });
    userList = temp
  });

  socket.on("sendMsg", function(data){
    addNewMsg(socket, data)
  })

  socket.on("readMsg", function(data){
    readMsg(socket, data)
  })

  socket.on("typing", function(data){
    typing(socket, data)
  })

  socket.on("getStatus", function(data){
    sendStatus(socket, data)
  })
  
});

// mysql connection
const mysql = require('mysql');
const host = 'localhost';
const user = 'root';
const password = '';
const database = 'dashboard';
const con = mysql.createConnection({
  host, user, password, database,
});
con.connect();

const addNewMsg = async (socket, data) => {
  const nowDate = new Date().toJSON().slice(0, 19).replace('T', ' ');
  var client = '';
  userList.map((user, index)=>{
    if(user.user_id == data.to_id){
      client = user.socket
    }
  })

  const query = "INSERT INTO chats (from_id, to_id, message, created_at) VALUES ('" + data.from_id + "', '"+data.to_id+"', '"+data.message+"', '"+nowDate+"')";
  console.log(query)
  var updated = ''
  await con.query(query, (err, result, fields) => {
    if (err) throw err;
  });
  const query1 = "select * from chats where (from_id = "+data.from_id+" and to_id = "+data.to_id+") or (to_id = "+data.from_id+" and from_id = "+data.to_id+")";

  con.query(query1, async (err, result, fields) => {
    if (err) throw err;

    socket.emit("newMessage", {
      message: result,
      client_id: data.to_id
    });
    var temp = result
    if(client){
      var query = "SELECT * FROM chats WHERE to_id = "+data.to_id+" AND read_at IS NULL";
      var unRead = ''
      await con.query(query, (err, result, fields) => {
        if (err) throw err;
        unRead = result
        client.emit("newMessage", {
          message: temp,
          client_id: data.from_id,
          newMsg: result.length,
          result: result
        });
      });
    }
  });
};

const readMsg = async (socket, data) => {
  const nowDate = new Date().toJSON().slice(0, 19).replace('T', ' ');
  var client = '';
  userList.map((user, index)=>{
    if(user.user_id == data.to_id){
      client = user.socket
    }
  })

  const query = "UPDATE chats SET read_at= '"+nowDate+"' WHERE from_id = "+data.to_id+" AND to_id = "+data.from_id+"";
  var updated = ''
  await con.query(query, (err, result, fields) => {
    if (err) throw err;
  });

  const query1 = "select * from chats where (from_id = "+data.from_id+" and to_id = "+data.to_id+") or (to_id = "+data.from_id+" and from_id = "+data.to_id+")";

  con.query(query1, async (err, result, fields) => {
    if (err) throw err;

    socket.emit("newUpdated", {
      message: result,
      client_id: data.to_id
    });
    var temp = result
    if(client){
      var query = "SELECT * FROM chats WHERE to_id = "+data.to_id+" AND read_at IS NULL";
      var unRead = ''
      await con.query(query, (err, result, fields) => {
        if (err) throw err;
        unRead = result
        client.emit("newUpdated", {
          message: temp,
          client_id: data.from_id,
          newMsg: result.length,
          result: result
        });
      });
    }
    
  });
};

const  typing = async (socket, data) => {
  const nowDate = new Date().toJSON().slice(0, 19).replace('T', ' ');
  var client = '';
  userList.map((user, index)=>{
    if(user.user_id == data.to_id){
      client = user.socket
    }
  })

  if(client){
    console.log("send typing")
    client.emit("userTyping", {
      client_id: data.from_id
    });
  }
};

const  sendStatus = async (socket, data) => {

  var client = '';
  userList.map((user, index)=>{
    if(user.user_id == data.to_id){
      client = user.socket
    }
  })

  if(client){
    console.log("send status")
    socket.emit("sendStatus", {
      flag: true,
      to_id: data.to_id
    });
  } else {
    console.log("send status")
    socket.emit("sendStatus", {
      flag: false,
      to_id: data.to_id
    });
  }
};