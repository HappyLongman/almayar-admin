<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::get('/getcategory', 'App\Http\Controllers\ActionController@getcategory');
Route::get('/getngo', 'App\Http\Controllers\ActionController@getngo');
Route::get('/help', 'App\Http\Controllers\ActionController@help');
Route::get('/donate', 'App\Http\Controllers\ActionController@donate');

// ======================= Login & Sign up =================================

Route::post('/getVerifyCode', 'ApiController@getVerifyCode');
Route::post('/phoneVerify', 'ApiController@phoneVerify');
Route::post('/SignUp', 'ApiController@SignUp');
Route::post('/SignUpWithAvatar', 'ApiController@SignUpWithAvatar');
Route::post('/Login', 'ApiController@Login');

Route::post('/getProfileInfo', 'ApiController@getProfileInfo');
Route::post('/getUserInfo', 'ApiController@getUserInfo');
Route::post('/toggleLike', 'ApiController@toggleLike');
Route::post('/getProDetail', 'ApiController@getProDetail');
Route::post('/getNextPage', 'ApiController@getNextPage');
Route::post('/getCategories', 'ApiController@getCategories');

Route::post('/addProduct', 'ApiController@addProduct');
Route::post('/editProduct', 'ApiController@editProduct');
Route::post('/getAllProducts', 'ApiController@getAllProducts');
Route::post('/toggleFollow', 'ApiController@toggleFollow');
Route::post('/editProfile', 'ApiController@editProfile');
Route::post('/upgradeMembership', 'ApiController@upgradeMembership');
Route::post('/getFollowers', 'ApiController@getFollowers');
Route::post('/getFollowings', 'ApiController@getFollowings');
Route::post('/editProductWithImage', 'ApiController@editProductWithImage');
Route::post('/getMessage', 'ApiController@getMessage');
Route::post('/getPrivateMessage', 'ApiController@getPrivateMessage');
Route::post('/searchUser', 'ApiController@searchUser');
