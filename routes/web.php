<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'HomeController@shopuserMain');
Auth::routes();

Route::get('/smsTest', 'BulkSmsController@sendSms')->name('smsTest');

Route::get('/send-notification', 'ActionController@sendOfferNotification');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Auth::routes();
// shopuser part 
Route::get('/shopusermain', 'HomeController@shopuserMain');
Route::get('/shopuserDetail/{id}', 'HomeController@shopuserDetail')->name('shopuserDetail');
// discount part
Route::get('/discountmain', 'HomeController@discountmain');
Route::get('/discountAddProduct', 'HomeController@discountAddProduct');
Route::get('/veiwDetailDiscount/{id}', 'HomeController@veiwDetailDiscount')->name('veiwDetailDiscount');
Route::post('/veiwDiscountDel', 'ActionController@veiwDiscountDel')->name('veiwDiscountDel');
// Route::post('/discountAdminSave', 'HomeController@discountAdminSave')->name('discountAdminSave');
Route::post('/discountAdminSave', 'ActionController@discountAdminSave')->name('discountAdminSave');
Route::post('/discountAdminUpdate', 'ActionController@discountAdminUpdate')->name('discountAdminUpdate');



Route::get('/offerMain', 'ActionController@offerMain')->name('offerMain');

Route::get('/ngomain', 'HomeController@mainNGO');
Route::get('/productmain', 'HomeController@productmain');
Route::get('/helpmain', 'HomeController@mainHelp');
Route::post('/donateDel', 'ActionController@donateDel')->name('donateDel');
//axios request
Route::get('/donate', 'ActionController@donate')->name('donate');
Route::get('/help', 'ActionController@help')->name('help');
Route::get('/getcategory', 'ActionController@getcategory')->name('getcategory');
Route::get('/getngo', 'ActionController@getngo')->name('getngo');

Route::get('/categoryPage/{page}', 'HomeController@categoryPage')->name('categoryPage');
Route::get('/productPage/{page}', 'HomeController@productPage')->name('productPage');
Route::get('/userPage/{page}', 'HomeController@userPage')->name('userPage');

Route::post('/userStatus', 'ActionController@userStatus')->name('userStatus');
Route::post('/productDraft', 'ActionController@productDraft')->name('productDraft');
Route::post('/productPublish', 'ActionController@productPublish')->name('productPublish');
Route::post('/actioinsUpdate', 'ActionController@actioinsUpdate')->name('actioinsUpdate');
Route::post('/actioins', 'ActionController@index')->name('actions');
Route::post('/ngos', 'ActionController@ngos')->name('ngos');
Route::post('/ngoUpdate', 'ActionController@ngoUpdate')->name('ngoUpdate');
Route::post('/update', 'ActionController@update')->name('actions');
Route::group(['middleware' => 'auth'], function () {
Route::resource('user', 'UserController', ['except' => ['show']]);
Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});

Route::group(['middleware' => 'auth'], function () {
	Route::get('{page}', ['as' => 'page.index', 'uses' => 'PageController@index']);
});


