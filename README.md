#change Logs


/*[26-Feb 5:24:12 AM][1278 ms]*/ ALTER TABLE `dashboard`.`products` ADD COLUMN `pro_likes` INT NULL AFTER `pro_status`; 

/*[26-Feb 5:42:01 AM][1683 ms]*/ ALTER TABLE `dashboard`.`notifications` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, DROP PRIMARY KEY, ADD PRIMARY KEY (`id`); 


/*[26-Feb 5:49:42 AM][613 ms]*/ CREATE TABLE `dashboard`.`chats` ( `id` INT NOT NULL AUTO_INCREMENT, `from_id` INT, `to_id` INT, `message` VARCHAR(50), PRIMARY KEY (`id`) );

/*[26-Feb 5:50:10 AM][174 ms]*/ ALTER TABLE `dashboard`.`chats` ADD COLUMN `read_at` TIMESTAMP NULL AFTER `message`, ADD COLUMN `created_at` TIMESTAMP NULL AFTER `read_at`, ADD COLUMN `updated_at` TIMESTAMP NULL AFTER `created_at`; 

/*[26-Feb 6:43:55 AM][715 ms]*/ CREATE TABLE `dashboard`.`likes` ( `id` INT NOT NULL AUTO_INCREMENT, `pro_id` INT, `u_id` INT, PRIMARY KEY (`id`) ); 

/*[26-Feb 6:47:28 AM][474 ms]*/ CREATE TABLE `dashboard`.`followings` ( `id` INT NOT NULL AUTO_INCREMENT, `u_id` INT, `follower_id` INT, PRIMARY KEY (`id`) ); 